package com.practicas.api.service;


import com.practicas.api.entity.TWarehouse;
import com.practicas.api.entity.TWhProduct;
import com.practicas.api.entity.TWhOffice;
import com.practicas.api.model.Response;

public interface WarehouseService {
	
	public Response getWarehouse(); 
	public Response createWarehouse(TWarehouse warehouse);
	public Response deleteWarehouse(Integer idWarehouse);
	public Response getProduct(); 
	public Response createProduct(TWhProduct product);
	public Response deleteProduct(TWhProduct product);
	public Response getOffice(); 
        public Response getWarehouseByOffice(Integer office); 
	public Response createOffice(TWhOffice office);
	public Response deleteOffice(Integer idWarehouseOffice);
	

}
