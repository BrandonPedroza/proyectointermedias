package com.practicas.api.service;

import com.practicas.api.entity.TCategory;
import com.practicas.api.entity.TProduct;
import com.practicas.api.entity.TProductList;
import com.practicas.api.entity.TProductcategory;
import com.practicas.api.entity.TSale;
import com.practicas.api.model.Response;

import java.util.List;

public interface ProductService {
	
	public List<TProduct> allProducts();
	Response getProduct();
	public Response getProductBy(Integer idprod);
	public Response createProduct(TProduct product);
	public Response deleteProduct(Integer product);
	
	
	public Response getProductListBy(Integer idSale);
	Response getProductList();	
	public Response createProductlist(TProductList list);
	public Response deleteProductlist(Integer idprodlist);

	public Response getSale();
	public Response createSale(TSale sale);
	public Response deleteSale(TSale sale);
	
	public Response getProductCategory(Integer idProduct);
	public Response createProductCategory(TProductcategory productcategory);
	public Response deleteProductCategory(Integer idCategory);
	
	public Response getCategory();
	public Response createCategory(TCategory category);
	public Response deleteCategory(Integer idCategory);
	
	public Response graphSales(String initDate, String endDate, int seller,int tipo);
	
	
}
