package com.practicas.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.practicas.api.entity.TWarehouse;
import com.practicas.api.entity.TWhProduct;
import com.practicas.api.entity.TWhOffice;
import com.practicas.api.model.Messages;
import com.practicas.api.model.Response;
import com.practicas.api.repository.WarehouseRepo;
import com.practicas.api.repository.WHOfficeRepo;
import com.practicas.api.repository.WHProductRepo;
import com.practicas.api.service.WarehouseService;

@Service("warehouseService")
public class WarehouseServiceImp implements WarehouseService{

	
    @Autowired
    WarehouseRepo repoWarehouse;

    @Autowired
    WHOfficeRepo repoOffice;

    @Autowired
    WHProductRepo repoProduct;

    @Override
    public Response getWarehouse() {
            Response rsp = new Response();
            rsp.setMsg("Bulk");
            rsp.setRecordset(repoWarehouse.findAll());
            return rsp;
    }
        
    @Override
    public Response createWarehouse(TWarehouse warehouse) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoWarehouse.save(warehouse));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }
		
        return rsp;
    
    }
	
    @Override
    public Response deleteWarehouse(Integer idWarehouse) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoWarehouse.deleteById(idWarehouse);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    
    }
	
    @Override
    public Response getProduct() {
        Response rsp = new Response();
        rsp.setCode(200);
        rsp.setMsg("Bulk");
        rsp.setRecordset(repoProduct.findAll());
        return rsp;
    }

    @Override
    public Response createProduct(TWhProduct product) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoProduct.save(product));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }


    @Override
    public Response deleteProduct(TWhProduct product) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoProduct.deleteById(product);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }

    @Override
    public Response getOffice() {
        Response rsp = new Response();
        rsp.setCode(200);
        rsp.setMsg("Bulk");
        rsp.setRecordset(repoOffice.findAll());
        return rsp;
    }
    
    @Override
    public Response getWarehouseByOffice(Integer idwh) {
            Response rsp = new Response();
    try {
            rsp.setCode(200);
            rsp.setMsg(Messages.OK_DATA.getValue());
            rsp.setRecordset(repoOffice.findByOffice(idwh));
    }catch(DataAccessException ex) {
            rsp.setCode(500);
            rsp.setMsg(Messages.NOT_DATA.getValue()+", "+ ex);
    }

    return rsp;

    }

    @Override
    public Response createOffice(TWhOffice office) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoOffice.save(office));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }

            return rsp;
    }

    @Override
    public Response deleteOffice(Integer idWarehouseOffice) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoOffice.deleteById(idWarehouseOffice);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }

}
