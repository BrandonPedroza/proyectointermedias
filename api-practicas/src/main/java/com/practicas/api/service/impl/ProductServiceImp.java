package com.practicas.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.practicas.api.entity.TCategory;
import com.practicas.api.entity.TProduct;
import com.practicas.api.entity.TProductList;
import com.practicas.api.entity.TProductcategory;
import com.practicas.api.entity.TSale;
import com.practicas.api.model.Messages;
import com.practicas.api.model.Response;
import com.practicas.api.repository.CategoryRepo;
import com.practicas.api.repository.PrdCategoryRepo;
import com.practicas.api.repository.ProductListRepo;
import com.practicas.api.repository.ProductRepo;
import com.practicas.api.repository.SaleRepo;
import com.practicas.api.service.ProductService;

@Service("ProductSrv")
public class ProductServiceImp implements ProductService {

	@Autowired
	ProductRepo  repoProduct;
	
	@Autowired
	ProductListRepo repoProductList;
	
	@Autowired
	SaleRepo repoSale;
	
	@Autowired
	PrdCategoryRepo repoPrdCategory;
	
	@Autowired
	CategoryRepo repoCategory;
	
	@Autowired
	JdbcTemplate jdbcTemp;
	
	@Override
	public List<TProduct> allProducts() {
		return repoProduct.findAll();
	}
	
	public Response getProduct() {
		Response rsp = new Response();
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoProduct.findAll());
		return rsp;
	}	
	
	@Override
	public Response getProductBy(Integer idProd) {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoProduct.findById(idProd));
		return rsp;
	}


	@Override
	public Response createProduct(TProduct product) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoProduct.save(product));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response deleteProduct(Integer product) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoProduct.deleteById(product);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response getProductListBy(Integer idSale) {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoProductList.findByidsale(idSale));
		return rsp;
	}



	@Override
	public Response getProductList() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoProductList.findAll());
		return rsp;
	}



	@Override
	public Response createProductlist(TProductList list) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoProductList.save(list));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response deleteProductlist(Integer idprodlist) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoProductList.deleteById(idprodlist);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response getSale() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoSale.findAll());
		return rsp;
	}



	@Override
	public Response createSale(TSale sale) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoSale.save(sale));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;		
	}



	@Override
	public Response deleteSale(TSale sale) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoSale.deleteById(sale);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response getProductCategory(Integer idProduct) {
            
            String queryUsrRole = "select pc.id_product_category as idproductcategory, c.detail, pc.id_category as idcategory, pc.id_product as idproduct from intermediasG11.product_category pc \n" +
"				left join intermediasG11.category c on c.id_category = pc.id_category \n" +
"				where pc.id_product  = ?";
		
		List<Map<String,Object>> rspQuery = jdbcTemp.queryForList(queryUsrRole,idProduct);
		
		
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(rspQuery);
		return rsp;
	}



	@Override
	public Response createProductCategory(TProductcategory productcategory) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoPrdCategory.save(productcategory));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response deleteProductCategory(Integer idCategory) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoPrdCategory.deleteById(idCategory);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response getCategory() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoCategory.findAll());
		return rsp;
	}



	@Override
	public Response createCategory(TCategory category) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoCategory.save(category));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}



	@Override
	public Response deleteCategory(Integer idCategory) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoCategory.deleteById(idCategory);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

	@Override
	public Response graphSales(String initDate, String endDate, int seller, int tipo) {
		String queryGraph = "select dte,nro, sum(valor) total from (\n" + 
				"select case when ? = 0 then  MONTHNAME(delivery_date) \n" + 
				"	        when ? = 1 then  WEEK(delivery_date)  \n" + 
				"	        else DAYNAME(delivery_date) end dte,\n" + 
				"	         case when ? = 0 then  MONTH(delivery_date) \n" + 
				"	        when ? = 1 then  WEEK(delivery_date)  \n" + 
				"	        else DAY(delivery_date) end nro, valor from sale sa\n" + 
				"right join product_list pl on pl.id_sale = sa.id_sale \n" + 
				"where DATE(sa.delivery_date) BETWEEN  ? and ?  and  (sa.id_seller  = ?  or  0 = ?)) total\n" + 
				"group by total.dte,total.nro\n" + 
				"order by total.nro";
		
		List<Map<String,Object>> map = jdbcTemp.queryForList(queryGraph, tipo,tipo,tipo,tipo,initDate,endDate,seller,seller);
		
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(map);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
		return rsp;
	}


}
