package com.practicas.api.service;


import com.practicas.api.entity.TPrivilege;
import com.practicas.api.entity.TRole;
import com.practicas.api.entity.TUser;
import com.practicas.api.entity.TUserRole;
import com.practicas.api.model.Response;

public interface UserService {
	
	public Response getUsers(); 
	public Response recoveryPass(String email);
	public Response createUser(TUser user);
	public Response deleteUser(Integer idUser);
	public Response login(String email,String pass);
	public Response getRoles(); 
	public Response createRole(TRole role);
	public Response deleteRole(Integer role);
	public Response getPrivilegeByRole(Integer id); 
	public Response createPrivilege(TPrivilege priv);
	public Response deletePrivilege(Integer priv);
	public Response getUserRoleByIdUser(Integer id); 
	public Response createUserRole(TUserRole userRole);
	public Response deleteUserRole(Integer userRole);
	public Response getUserRoleAll(Integer idRole); 

}
