package com.practicas.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.practicas.api.entity.TTransfer;
import com.practicas.api.entity.TLogTransfer;
import com.practicas.api.entity.TTransferProduct;
import com.practicas.api.model.Messages;
import com.practicas.api.model.Response;
import com.practicas.api.repository.TransferRepo;
import com.practicas.api.repository.LogTransferRepo;
import com.practicas.api.repository.TransferProductRepo;
import com.practicas.api.service.TransferService;

@Service("transferService")
public class TransferServiceImp implements TransferService{

	
    @Autowired
    TransferRepo repoTransfer;

    @Autowired
    LogTransferRepo repoLogTransfer;
    
    @Autowired
    TransferProductRepo repoTransferProduct;

    @Override
    public Response getTransfer() {
        Response rsp = new Response();
        rsp.setMsg("Bulk");
        rsp.setRecordset(repoTransfer.findAll());
        return rsp;
    }

    @Override
    public Response createTransfer(TTransfer transfer) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoTransfer.save(transfer));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }

        return rsp;

    }
    @Override
    public Response deleteTransfer(TTransfer transfer) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoTransfer.deleteById(transfer);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;

    }   

    @Override
    public Response getLogTransfer() {
        Response rsp = new Response();
        rsp.setCode(200);
        rsp.setMsg("Bulk");
        rsp.setRecordset(repoLogTransfer.findAll());
        return rsp;
    }

    @Override
    public Response createLogTransfer(TLogTransfer log) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoLogTransfer.save(log));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }


    @Override
    public Response deleteLogTransfer(TLogTransfer log) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoLogTransfer.deleteById(log);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }

    
    @Override
    public Response getTransferProduct() {
        Response rsp = new Response();
        rsp.setCode(200);
        rsp.setMsg("Bulk");
        rsp.setRecordset(repoTransferProduct.findAll());
        return rsp;
    }

    @Override
    public Response createTransferProduct(TTransferProduct log) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.CREATE_OK.getValue());
                rsp.setRecordset(repoTransferProduct.save(log));
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }


    @Override
    public Response deleteTransferProduct(TTransferProduct log) {
        Response rsp = new Response();
        try {
                rsp.setCode(200);
                rsp.setMsg(Messages.DELETE_OK.getValue());
                repoTransferProduct.delete(log);
        }catch(DataAccessException ex) {
                rsp.setCode(500);
                rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
        }

        return rsp;
    }

}
