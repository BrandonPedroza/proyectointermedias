package com.practicas.api.service;


import com.practicas.api.entity.TTransfer;
import com.practicas.api.entity.TLogTransfer;
import com.practicas.api.entity.TTransferProduct;
import com.practicas.api.model.Response;

public interface TransferService {
	
	public Response getTransfer(); 
	public Response createTransfer(TTransfer transfer);
	public Response deleteTransfer(TTransfer transfer);
	public Response getLogTransfer(); 
	public Response createLogTransfer(TLogTransfer log);
	public Response deleteLogTransfer(TLogTransfer log);
        public Response getTransferProduct(); 
	public Response createTransferProduct(TTransferProduct log);
	public Response deleteTransferProduct(TTransferProduct log);
	

}