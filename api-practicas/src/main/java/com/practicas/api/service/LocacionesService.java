package com.practicas.api.service;


import com.practicas.api.entity.Tcity;
import com.practicas.api.entity.Tcustomer;
import com.practicas.api.entity.Tinnventory;
import com.practicas.api.entity.Toffice;
import com.practicas.api.entity.TState;

import com.practicas.api.model.Response;

public interface LocacionesService {
    
    //--> Office
	public Response getOffices(); 
	public Response createOffice(Toffice office);
    public Response deleteOffice(Integer idOffice);
    //--> City
	public Response getCity(); 
	public Response createCity(Tcity city);
    public Response deleteCity(Integer city);
    //--> State
	public Response getState(); 
	public Response createState(TState state);
    public Response deleteState(Integer state);
    //--> Customer
    public Response getCustomer(); 
	public Response createCustomer(Tcustomer customer);
	public Response deleteCustomer(Integer customer);
	public Response getCustomerByNit(String nit);
    //--> Innventory
    public Response getInnventory(); 
	public Response createInnventory(Tinnventory innventory);
	public Response deleteInnventory(Integer innventory);
	

}
