package com.practicas.api.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.practicas.api.component.MailsG11;
import com.practicas.api.entity.TPrivilege;
import com.practicas.api.entity.TRole;
import com.practicas.api.entity.TUser;
import com.practicas.api.entity.TUserRole;
import com.practicas.api.model.Messages;
import com.practicas.api.model.Response;
import com.practicas.api.repository.PrivilegeRepo;
import com.practicas.api.repository.RoleRepo;
import com.practicas.api.repository.UserRepo;
import com.practicas.api.repository.UserRoleRepo;
import com.practicas.api.service.UserService;

@Service("userService")
public class UserServiceImp implements UserService{

	
	@Autowired
	UserRepo repoUsers;
	
	@Autowired
	RoleRepo repoRole;
	
	@Autowired
	PrivilegeRepo repoPriv;
	
	@Autowired
	UserRoleRepo repoUsrRole;
	
	@Autowired
	JdbcTemplate jdbcTemp;
	
	@Autowired
	MailsG11 mails;

	@Override
	public Response getUsers() {
		Response rsp = new Response();
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoUsers.findAll());
		return rsp;
	}

	public Response createUser(TUser user) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoUsers.save(user));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
		
		}
	
	public Response deleteUser(Integer user) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoUsers.deleteById(user);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
		
		}


	@Override
	public Response recoveryPass(String email) {
		Response rsp = new Response();
		String queryMail = "select * from user where mail = ?";
		Map<String, Object> rspQuery = jdbcTemp.queryForMap(queryMail,email);
		
		if (rspQuery.isEmpty()) {
			 rsp.setCode(200);
			 rsp.setMsg(Messages.NOT_DATA.getValue());
		}else {
			 mails.sendSimpleMessage(rspQuery.get("mail").toString(),
					 				 rspQuery.get("name").toString(),
					 				 rspQuery.get("password").toString());
			 rsp.setCode(200);
			 rsp.setMsg(Messages.MAIL.getValue());
		}
		
		
		return rsp;
	}
	
	public Response login(String email,String pass) {
		Response rsp = new Response();
		String queryMail = "select * from user where mail = ? and password = ?";
		List<Map<String, Object>> rspQuery = jdbcTemp.queryForList(queryMail,email,pass);
		String roleUser = "select ur.id_role, r.detail roleName from user_role ur\n" + 
				"left join role r on r.id_role = ur.id_role\n" + 
				"where ur.id_user = ?";
		if (rspQuery.isEmpty()) {
			 rsp.setCode(204);
			 rsp.setMsg(Messages.FAIL.getValue());
		}else {
			 rsp.setCode(200);
			 rsp.setMsg(Messages.SUCCESS.getValue());
			 Map<String,Object> rolesMap = new HashMap<>();
			 List<Map<String,Object>> roles = jdbcTemp.queryForList(roleUser,rspQuery.get(0).get("id_user"));
			 rolesMap.put("roles", roles);
			 List<Map<String, Object>> data = rspQuery;
			 data.get(0).remove("password");			 
			 rolesMap.put("datos", data);
			 rsp.setRecordset(rolesMap);
		}
		return rsp;
	}

	
	@Override
	public Response getRoles() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoRole.findAll());
		return rsp;
	}

	@Override
	public Response createRole(TRole role) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoRole.save(role));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deleteRole(Integer role) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoRole.deleteById(role);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response createPrivilege(TPrivilege priv) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoPriv.save(priv));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deletePrivilege(Integer priv) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoPriv.deleteById(priv);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

	@Override
	public Response getPrivilegeByRole(Integer id) {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoPriv.findByIdRole(id));
		return rsp;
	}

	@Override
	public Response getUserRoleByIdUser(Integer id) {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(repoUsrRole.findByIdUser(id));
		return rsp;
	}

	@Override
	public Response createUserRole(TUserRole userRole) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(repoUsrRole.save(userRole));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

	@Override
	public Response deleteUserRole(Integer userRole) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			repoUsrRole.deleteById(userRole);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}
	

	public Response getUserRoleAll(Integer idRole) {
		String queryUsrRole = "select ur.id_user, usr.name, ur.id_role from user_role ur\n" + 
				"left join user usr on usr.id_user = ur.id_user \n" + 
				"where id_role  = ?";
		
		List<Map<String,Object>> rspQuery = jdbcTemp.queryForList(queryUsrRole,idRole);
		
		
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(rspQuery);
		return rsp;
	}

	
	

}
