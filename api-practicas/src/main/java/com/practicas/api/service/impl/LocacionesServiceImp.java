package com.practicas.api.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.practicas.api.component.MailsG11;

import com.practicas.api.entity.Tcity;
import com.practicas.api.entity.Tcustomer;
import com.practicas.api.entity.Tinnventory;
import com.practicas.api.entity.Toffice;
import com.practicas.api.entity.TState;

import com.practicas.api.model.Messages;
import com.practicas.api.model.Response;

import com.practicas.api.repository.CityRepo;
import com.practicas.api.repository.CustomerRepo;
import com.practicas.api.repository.InnventoryRepo;
import com.practicas.api.repository.OfficeRepo;
import com.practicas.api.repository.StateRepo;

import com.practicas.api.service.LocacionesService;

@Service("LocacionesService")
public class LocacionesServiceImp implements LocacionesService{

	
	@Autowired
	CityRepo RepoCity;
	
	@Autowired
	CustomerRepo RepoCustomer;
	
	@Autowired
	InnventoryRepo RepoInnventory;
	
	@Autowired
    OfficeRepo RepoOffice;
    
    @Autowired
	StateRepo RepoState;
	
	@Autowired
	JdbcTemplate jdbcTemp;
	
	@Autowired
	MailsG11 mails;

    //--> Office
	@Override
	public Response getOffices() {
		Response rsp = new Response();
		rsp.setMsg("Bulk");
		rsp.setRecordset(RepoOffice.findAll());
		return rsp;
	}

	public Response createOffice(Toffice office) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoOffice.save(office));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
		
	}
	
	public Response deleteOffice(Integer idOffice) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			RepoOffice.deleteById(idOffice);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
		
	}

    //--> City
	@Override
	public Response getCity() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(RepoCity.findAll());
		return rsp;
	}

	@Override
	public Response createCity(Tcity city) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoCity.save(city));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deleteCity(Integer city) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			RepoCity.deleteById(city);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

    //--> State
    @Override
	public Response getState() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(RepoState.findAll());
		return rsp;
    }
    
	@Override
	public Response createState(TState state) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoState.save(state));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deleteState(Integer state) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			RepoState.deleteById(state);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

    //--> Customer
    @Override
    public Response getCustomer() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(RepoCustomer.findAll());
		return rsp;
    }
    
	@Override
	public Response createCustomer(Tcustomer customer) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoCustomer.save(customer));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deleteCustomer(Integer customer) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			RepoCustomer.deleteById(customer);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}
    
    //--> Innventory
	@Override
	public Response getInnventory() {
		Response rsp = new Response();
		rsp.setCode(200);
		rsp.setMsg("Bulk");
		rsp.setRecordset(RepoInnventory.findAll());
		return rsp;
    }
    
	@Override
	public Response createInnventory(Tinnventory innventory) {
		Response rsp = new Response();
		try {
			rsp.setCode(201);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoInnventory.save(innventory));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}


	@Override
	public Response deleteInnventory(Integer innventory) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.DELETE_OK.getValue());
			RepoInnventory.deleteById(innventory);
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.DELETE_NOK.getValue()+", "+ ex);
		}
		
			return rsp;
	}

	@Override
	public Response getCustomerByNit(String nit) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg(Messages.CREATE_OK.getValue());
			rsp.setRecordset(RepoCustomer.findByNit(nit));
		}catch(DataAccessException ex) {
			rsp.setCode(500);
			rsp.setMsg(Messages.CREATE_NOK.getValue()+", "+ ex);
		}
		
		return rsp;
	}


	
	

}
