package com.practicas.api.model;

public enum Messages {
	ERROR_ON("Error on"),
	NOT_DATA("No Data Found"),
	FAIL("Credenciales no coinciden"),
	SUCCESS("Bienvenido"),
	ERROR_REG("Error al crear/actualizar registro"),
	CREATE_OK("Se creó/actualizó registro"),
	CREATE_NOK("NO creó/actualizó registro"),
	DELETE_OK("Se eliminó registro"),
	DELETE_NOK("No eliminó registro"),
	MAIL("Email sended"),
        OK_DATA("Data Find"),
	OUTLABEL("outMsg");

	Messages(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}
}
