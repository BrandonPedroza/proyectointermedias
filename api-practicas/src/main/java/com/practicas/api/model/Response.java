package com.practicas.api.model;

import java.io.Serializable;


import lombok.Data;

@Data
public class Response implements Serializable {


	private static final long serialVersionUID = 1261651034155744403L;
	private long code;
	private String msg;
	private String type;
	private Object recordset;
}