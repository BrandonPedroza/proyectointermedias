package com.practicas.api.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.practicas.api.service.ProductService;
import com.practicas.api.entity.TCategory;
import com.practicas.api.entity.TProduct;
import com.practicas.api.entity.TProductList;
import com.practicas.api.entity.TProductcategory;
import com.practicas.api.entity.TSale;
import com.practicas.api.model.Response;
import org.springframework.web.bind.annotation.PathVariable;


@Controller
@RequestMapping("/product")
public class productController {
	
	
	@Autowired
	ProductService prodSrv;
	
	
	private static final Logger LOG =	LoggerFactory.getLogger(productController.class);

	
	
	
	/* CRUD PRODUCT */
	@GetMapping(value="/getAllProducts")
	public ResponseEntity<Response> getAllProducts() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(prodSrv.allProducts());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}			
	}
	
	@GetMapping(value="/getProductBy/{idProd}")
	public ResponseEntity<Response> getProductsBy(@PathVariable Integer idProd) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(prodSrv.getProductBy(idProd));
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}			
	}
	
	@PostMapping(value="/Product")
	public ResponseEntity<Response> createProduct(@RequestBody TProduct product) {
		
		Response rsp = prodSrv.createProduct(product);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@PutMapping(value="/Product")
	public ResponseEntity<Response> updateProduct(@RequestBody TProduct product) {
		
		Response rsp = prodSrv.createProduct(product);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	@DeleteMapping(value="/Product/{idProduct}")
	public ResponseEntity<Response> deleteProduct(@PathVariable Integer idProduct) {
		
		Response rsp = prodSrv.deleteProduct(idProduct);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
		
		
		/* CRUD PRODUCT LIST*/
		@GetMapping(value="/getAllProductLists")
		public ResponseEntity<Response> getAllProductList() {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.getProductList(), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}		
		}
		
		@GetMapping(value="/getAllProductListsBySale/{idSale}")
		public ResponseEntity<Response> getAllProductListBy(@PathVariable Integer idSale) {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.getProductListBy(idSale), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}		
		}
		
		
		@PostMapping(value="/productlist")
		public ResponseEntity<Response> createProductlist(@RequestBody TProductList list) {
			
			Response rsp = prodSrv.createProductlist(list);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error Create ProductList " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@PutMapping(value="/productlist")
		public ResponseEntity<Response> updateProductlist(@RequestBody TProductList list) {
			
			Response rsp = prodSrv.createProductlist(list);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error update ProductList " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		@DeleteMapping(value="/productlist")
		public ResponseEntity<Response> deleteProductlist(@RequestParam Integer idProductList) {
			
			Response rsp = prodSrv.deleteProductlist(idProductList);
			
			try {
				if(rsp.getCode() == 200) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error delete ProductList " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		
		
		/* CRUD SALE*/
		@GetMapping(value="/getAllSale")
		public ResponseEntity<Response> getAllSale() {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.getSale(), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		@PostMapping(value="/sale")
		public ResponseEntity<Response> createSale(@RequestBody TSale sale) {
			
			Response rsp = prodSrv.createSale(sale);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error Create Sale " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@PutMapping(value="/sale")
		public ResponseEntity<Response> updateSale(@RequestBody TSale sale) {
			
			Response rsp = prodSrv.createSale(sale);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error update Sale " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@DeleteMapping(value="/sale")
		public ResponseEntity<Response> deleteSale(@RequestParam TSale sale) {
			
			Response rsp = prodSrv.deleteSale(sale);
			
			try {
				if(rsp.getCode() == 200) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error delete Sale " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		
		
		/* CRUD PRODUCT CATEGORY*/
		@GetMapping(value="/getAllProductCategory/{idProduct}")
		public ResponseEntity<Response> getAllProductCategory(@PathVariable Integer idProduct) {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.getProductCategory(idProduct), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		@PostMapping(value="/productcategory")
		public ResponseEntity<Response> createProductCategory(@RequestBody TProductcategory productcategory) {
			
			Response rsp = prodSrv.createProductCategory(productcategory);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error Create ProductCategory " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@PutMapping(value="/productcategory")
		public ResponseEntity<Response> updateProductCategory(@RequestBody TProductcategory productcategory) {
			
			Response rsp = prodSrv.createProductCategory(productcategory);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error update ProductCategory " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@DeleteMapping(value="/productcategory/{idCategory}")
		public ResponseEntity<Response> deleteProductCategory(@PathVariable Integer idCategory) {
			
			Response rsp = prodSrv.deleteProductCategory(idCategory);
			
			try {
				if(rsp.getCode() == 200) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error delete ProductCategory" , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		
		
		
		/* CRUD CATEGORY*/
		@GetMapping(value="/getAllCategory")
		public ResponseEntity<Response> getAllCategory() {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.getCategory(), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		@PostMapping(value="/category")
		public ResponseEntity<Response> createCategory(@RequestBody TCategory category) {
			
			Response rsp = prodSrv.createCategory(category);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error Create Category " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@PutMapping(value="/category")
		public ResponseEntity<Response> updateCategory(@RequestBody TCategory category) {
			
			Response rsp = prodSrv.createCategory(category);
			
			try {
				if(rsp.getCode() == 201) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error update Category " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}	
		}
		
		@DeleteMapping(value="/category")
		public ResponseEntity<Response> deleteCategory(@RequestParam Integer idCategory) {
			
			Response rsp = prodSrv.deleteCategory(idCategory);
			
			try {
				if(rsp.getCode() == 200) {
					return new ResponseEntity<>(rsp, HttpStatus.OK);	
				}else {
					return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}catch(Exception ex)
			{
				LOG.error("Error delete Category" , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
		
		@GetMapping(value="/getGraph")
		public ResponseEntity<Response> getGraph(@RequestParam String init, @RequestParam String end, @RequestParam int por, @RequestParam int seller) {
			Response rsp = new Response();
			
			try {
				return new ResponseEntity<>(prodSrv.graphSales(init,end, seller, por), HttpStatus.OK);
			}catch(Exception ex)
			{
				LOG.error("ErrorGetting " , ex );
				rsp.setCode(500);
				rsp.setMsg("Error");
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
			}			
		}
}

