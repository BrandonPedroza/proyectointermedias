package com.practicas.api.controller;



import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.practicas.api.entity.TWarehouse;
import com.practicas.api.entity.TWhProduct;
import com.practicas.api.entity.TWhOffice;
import com.practicas.api.model.Response;
import com.practicas.api.service.WarehouseService;


@Controller
@RequestMapping("/warehouse")
public class warehouseController {
	
	@Autowired
	WarehouseService serviceWarehouse;
	
	private static final Logger LOG =	LoggerFactory.getLogger(warehouseController.class);
	
    /* CRUD WAREHOUSE */

	@GetMapping(value="/getAllWarehouse")
	public ResponseEntity<Response> getAll() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceWarehouse.getWarehouse());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createWarehouse")
	public ResponseEntity<Response> createWarehouse(@RequestBody TWarehouse warehouse) {
		
		Response rsp = serviceWarehouse.createWarehouse(warehouse);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorCreatingWarehouse " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateWarehouse")
	public ResponseEntity<Response> updateWarehouse(@RequestBody TWarehouse warehouse) {
		
		Response rsp = serviceWarehouse.createWarehouse(warehouse);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingWarehouse " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteWarehouse/{idWarehouse}")
	public ResponseEntity<Response> deleteWarehouse(@PathVariable Integer idWarehouse) {
		
		Response rsp = serviceWarehouse.deleteWarehouse(idWarehouse);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingWarehouse " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

	/* CRUD PRODUCT */
	
	@GetMapping(value="/getAllProduct")
	public ResponseEntity<Response> getAllProduct() {
		Response rsp = new Response();
		try {
                    return new ResponseEntity<>(serviceWarehouse.getProduct(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createProduct")
	public ResponseEntity<Response> createProduct(@RequestBody TWhProduct product) {
		
		Response rsp = serviceWarehouse.createProduct(product);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorCreatingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateProduct")
	public ResponseEntity<Response> updateProduct(@RequestBody TWhProduct product) {
		
		Response rsp = serviceWarehouse.createProduct(product);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteProduct")
	public ResponseEntity<Response> deleteProduct(@RequestParam TWhProduct product) {
		
		Response rsp = serviceWarehouse.deleteProduct(product);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingProduct " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

	/* CRUD OFFICE */
	
	@GetMapping(value="/getAllOffice")
	public ResponseEntity<Response> getAllOffice() {
		Response rsp = new Response();
		try {
			return new ResponseEntity<>(serviceWarehouse.getOffice(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
        
        @GetMapping(value="/getAllWarehouseOffice/{office}")
	public ResponseEntity<Response> getAllWarehouseOffice(@PathVariable Integer office) {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceWarehouse.getWarehouseByOffice(office));
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createOffice")
	public ResponseEntity<Response> createOffice(@RequestBody TWhOffice office) {
		
		Response rsp = serviceWarehouse.createOffice(office);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorCreatingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateOffice")
	public ResponseEntity<Response> updateOffice(@RequestBody TWhOffice office) {
		
		Response rsp = serviceWarehouse.createOffice(office);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteOffice/{idWarehouseOffice}")
	public ResponseEntity<Response> deleteOffice(@PathVariable Integer idWarehouseOffice) {
		
		Response rsp = serviceWarehouse.deleteOffice(idWarehouseOffice);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
}
