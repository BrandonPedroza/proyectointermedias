package com.practicas.api.controller;



import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.practicas.api.entity.TPrivilege;
import com.practicas.api.entity.TRole;
import com.practicas.api.entity.TUser;
import com.practicas.api.entity.TUserRole;
import com.practicas.api.model.Response;
import com.practicas.api.service.UserService;


@Controller
@RequestMapping("/users")
public class userController {
	
	@Autowired
	UserService serviceUser;
	
	
	
	private static final Logger LOG =	LoggerFactory.getLogger(userController.class);
	
	@GetMapping(value="/getAllUsers")
	public ResponseEntity<Response> getAll() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceUser.getUsers());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	@PostMapping(value="/recoveryPass")
	public ResponseEntity<Response> Pass(@RequestParam String email) {
		
		Response rsp = new Response();
		
		try {
			
			return new ResponseEntity<>(serviceUser.recoveryPass(email), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingUser " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/user")
	public ResponseEntity<Response> createUser(@RequestBody TUser user) {
		
		Response rsp = serviceUser.createUser(user);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingUser " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/user")
	public ResponseEntity<Response> updateUser(@RequestBody TUser user) {
		
		Response rsp = serviceUser.createUser(user);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingUser " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/user")
	public ResponseEntity<Response> deleteUser(@RequestParam Integer idUser) {
		
		Response rsp = serviceUser.deleteUser(idUser);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingUser " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@GetMapping(value="/login")
	public ResponseEntity<Response> login(@RequestParam  String user , @RequestParam String pass) {
		Response rsp = serviceUser.login(user, pass);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingUser " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	/* CRUD ROLE*/
	
	@GetMapping(value="/getAllRoles")
	public ResponseEntity<Response> getAllRoles() {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceUser.getRoles(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/role")
	public ResponseEntity<Response> createRole(@RequestBody TRole role) {
		
		Response rsp = serviceUser.createRole(role);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create Role " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/role")
	public ResponseEntity<Response> updateRole(@RequestBody TRole role) {
		
		Response rsp = serviceUser.createRole(role);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update Role " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/role")
	public ResponseEntity<Response> deleteRole(@RequestParam Integer idRole) {
		
		Response rsp = serviceUser.deleteRole(idRole);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete Role " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	
	
	/* CRUD PRIVILEGE*/
	
	
	@GetMapping(value="/getPrivilegeByRole/{idRole}")
	public ResponseEntity<Response> getPrivilegesBy(@PathVariable Integer idRole) {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceUser.getPrivilegeByRole(idRole), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/privilege")
	public ResponseEntity<Response> createPriv(@RequestBody TPrivilege priv) {
		
		Response rsp = serviceUser.createPrivilege(priv);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create Privilege " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/privilege")
	public ResponseEntity<Response> updatePrivilege(@RequestBody TPrivilege priv) {
		
		Response rsp = serviceUser.createPrivilege(priv);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update Priv " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/privilege")
	public ResponseEntity<Response> deleteRolePriv(@RequestParam Integer idPriv) {
		
		Response rsp = serviceUser.deletePrivilege(idPriv);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete Privilege " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	/* CRUD USER ROLE */ 
	
	@GetMapping(value="/getRolesByUser/{idUser}")
	public ResponseEntity<Response> getRolesByUser(@PathVariable Integer idUser) {
		Response rsp = new Response();
		try {
			return new ResponseEntity<>(serviceUser.getUserRoleByIdUser(idUser), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/userRole")
	public ResponseEntity<Response> createUserRole(@RequestBody TUserRole userRole) {
		
		Response rsp = serviceUser.createUserRole(userRole);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create Privilege " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/userRole")
	public ResponseEntity<Response> updateUserRole(@RequestBody TUserRole userRole) {
		
		Response rsp = serviceUser.createUserRole(userRole);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update Priv " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/userRole")
	public ResponseEntity<Response> deleteUserRole(@RequestParam Integer idUserRole) {
		
		Response rsp = serviceUser.deleteUserRole(idUserRole);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete Privilege " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@GetMapping(value="/getRolesUser/{idRole}")
	public ResponseEntity<Response> getRolesUser(@PathVariable Integer idRole) {
		Response rsp = new Response();
		try {
			return new ResponseEntity<>(serviceUser.getUserRoleAll(idRole), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	

}
