package com.practicas.api.controller;



import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.practicas.api.entity.Tcity;
import com.practicas.api.entity.Tcustomer;
import com.practicas.api.entity.Tinnventory;
import com.practicas.api.entity.Toffice;
import com.practicas.api.entity.TState;

import com.practicas.api.model.Response;
import com.practicas.api.service.LocacionesService;


@Controller
@RequestMapping("/locaciones")
public class LocacionesController {
	
	@Autowired
	LocacionesService serviceLocacion;
	
	
	
	private static final Logger LOG =	LoggerFactory.getLogger(LocacionesController.class);
    
    /* CRUD OFFICE */
	@GetMapping(value="/getAllOffices")
	public ResponseEntity<Response> getOffices() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceLocacion.getOffices());
			
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	@PostMapping(value="/createOffice")
	public ResponseEntity<Response> createOffice(@RequestBody Toffice office) {
		
		Response rsp = serviceLocacion.createOffice(office);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorCreatingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateOffice")
	public ResponseEntity<Response> updateOffice(@RequestBody Toffice office) {
		
		Response rsp = serviceLocacion.createOffice(office);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteOffice/{idOffice}")
	public ResponseEntity<Response> deleteOffice(@PathVariable Integer idOffice) {
		
		Response rsp = serviceLocacion.deleteOffice(idOffice);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingOffice " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	/* CRUD CITY*/
	
	@GetMapping(value="/getAllCities")
	public ResponseEntity<Response> getAllCities() {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceLocacion.getCity(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingCities " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/createCity")
	public ResponseEntity<Response> createCity(@RequestBody Tcity city) {
		
		Response rsp = serviceLocacion.createCity(city);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create City " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/updateCity")
	public ResponseEntity<Response> updateCity(@RequestBody Tcity city) {
		
		Response rsp = serviceLocacion.createCity(city);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update City " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/deleteCity")
	public ResponseEntity<Response> deleteCity(@RequestParam Integer idCity) {
		
		Response rsp = serviceLocacion.deleteCity(idCity);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete city " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	
	
	/* CRUD STATE*/
	
	@GetMapping(value="/getAllStates")
	public ResponseEntity<Response> getAllStates() {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceLocacion.getState(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingStates " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	@PostMapping(value="/createState")
	public ResponseEntity<Response> createState(@RequestBody TState state) {
		
		Response rsp = serviceLocacion.createState(state);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create State " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/updateState")
	public ResponseEntity<Response> updateState(@RequestBody TState state) {
		
		Response rsp = serviceLocacion.createState(state);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update State " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/deleteState")
	public ResponseEntity<Response> deleteState(@RequestParam Integer idState) {
		
		Response rsp = serviceLocacion.deleteState(idState);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete State " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	/* CRUD CUSTOMER */ 
	
	@GetMapping(value="/getAllCustomers")
	public ResponseEntity<Response> getAllCustomers() {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceLocacion.getCustomer(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingCustomers " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@GetMapping(value="/getCustomer/{nit}")
	public ResponseEntity<Response> getCustomers(@PathVariable String nit) {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceLocacion.getCustomerByNit(nit), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingCustomers " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/createCustomer")
	public ResponseEntity<Response> createCustomer(@RequestBody Tcustomer customer) {
		
		Response rsp = serviceLocacion.createCustomer(customer);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create Customer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/updateCustomer")
	public ResponseEntity<Response> updateCustomer(@RequestBody Tcustomer customer) {
		
		Response rsp = serviceLocacion.createCustomer(customer);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update Customer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/deleteCustomer")
	public ResponseEntity<Response> deleteCustomer(@RequestParam Integer idCustomer) {
		
		Response rsp = serviceLocacion.deleteCustomer(idCustomer);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete Customer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	/* CRUD INNVENTORY */ 
	
	@GetMapping(value="/getAllInnventories")
	public ResponseEntity<Response> getAllInnventories() {
		Response rsp = new Response();
		
		try {
			return new ResponseEntity<>(serviceLocacion.getInnventory(), HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingCustomers " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PostMapping(value="/createInnventory")
	public ResponseEntity<Response> createInnventory(@RequestBody Tinnventory innventory) {
		
		Response rsp = serviceLocacion.createInnventory(innventory);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error Create Customer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@PutMapping(value="/updateInnventory")
	public ResponseEntity<Response> updateInnventory(@RequestBody Tinnventory innventory) {
		
		Response rsp = serviceLocacion.createInnventory(innventory);
		
		try {
			if(rsp.getCode() == 201) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error update Innventory " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	@DeleteMapping(value="/deleteInnventory")
	public ResponseEntity<Response> deleteInnventory(@RequestParam Integer idInnventory) {
		
		Response rsp = serviceLocacion.deleteInnventory(idInnventory);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("Error delete Innventory " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

}
