package com.practicas.api.controller;



import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.practicas.api.entity.TTransfer;
import com.practicas.api.entity.TLogTransfer;
import com.practicas.api.entity.TTransferProduct;
import com.practicas.api.model.Response;
import com.practicas.api.service.TransferService;


@Controller
@RequestMapping("/transfer")
public class transferController {
	
	@Autowired
	TransferService serviceTransfer;
	
	private static final Logger LOG =	LoggerFactory.getLogger(transferController.class);
	
    /* CRUD TRANSFER */

	@GetMapping(value="/getAllTransfer")
	public ResponseEntity<Response> getAll() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceTransfer.getTransfer());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createTransfer")
	public ResponseEntity<Response> createTransfer(@RequestBody TTransfer transfer) {
		
		Response rsp = serviceTransfer.createTransfer(transfer);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorCreatringTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateTransfer")
	public ResponseEntity<Response> updateTransfer(@RequestBody TTransfer transfer) {
		
		Response rsp = serviceTransfer.createTransfer(transfer);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteTransfer")
	public ResponseEntity<Response> deleteTransfer(@RequestParam TTransfer transfer) {
		
		Response rsp = serviceTransfer.deleteTransfer(transfer);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

	/* CRUD LOG */
	
	@GetMapping(value="/getAllLog")
	public ResponseEntity<Response> getAllLog() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceTransfer.getLogTransfer());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createLog")
	public ResponseEntity<Response> createLogTransfer(@RequestBody TLogTransfer log) {
		
		Response rsp = serviceTransfer.createLogTransfer(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateLog")
	public ResponseEntity<Response> updateLogTransfer(@RequestBody TLogTransfer log) {
		
		Response rsp = serviceTransfer.createLogTransfer(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteLog")
	public ResponseEntity<Response> deleteLogTransfer(@RequestParam TLogTransfer log) {
		
		Response rsp = serviceTransfer.deleteLogTransfer(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
        
        /* CRUD PRODUCT */
	
	@GetMapping(value="/getAllProduct")
	public ResponseEntity<Response> getAllProduct() {
		Response rsp = new Response();
		try {
			rsp.setCode(200);
			rsp.setMsg("OK");
			rsp.setRecordset(serviceTransfer.getTransferProduct());
			// Agregar validaciones.
			return new ResponseEntity<>(rsp, HttpStatus.OK);
		}catch(Exception ex)
		{
			LOG.error("ErrorGetting" , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
	@PostMapping(value="/createProduct")
	public ResponseEntity<Response> createProduct(@RequestBody TTransferProduct log) {
		
		Response rsp = serviceTransfer.createTransferProduct(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorGettingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@PutMapping(value="/updateProduct")
	public ResponseEntity<Response> updateProduct(@RequestBody TTransferProduct log) {
		
		Response rsp = serviceTransfer.createTransferProduct(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorUpdatingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	@DeleteMapping(value="/deleteProduct")
	public ResponseEntity<Response> deleteProduct(@RequestBody TTransferProduct log) {
		
		Response rsp = serviceTransfer.deleteTransferProduct(log);
		
		try {
			if(rsp.getCode() == 200) {
				return new ResponseEntity<>(rsp, HttpStatus.OK);	
			}else {
				return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);	
			}
		}catch(Exception ex)
		{
			LOG.error("ErrorDeletingLogTransfer " , ex );
			rsp.setCode(500);
			rsp.setMsg("Error");
			return new ResponseEntity<>(rsp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
    
}
