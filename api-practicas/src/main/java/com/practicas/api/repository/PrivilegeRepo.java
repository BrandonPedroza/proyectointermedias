package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TPrivilege;

@Repository("privRepository")
public interface PrivilegeRepo extends JpaRepository<TPrivilege, Serializable> {
	
	public abstract List<TPrivilege> findByIdRole(Integer idRole); 

}
