package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TProduct;

@Repository("ProductRepository")
public interface ProductRepo extends JpaRepository<TProduct, Serializable> {

	public abstract List<TProduct> findByIdProduct(Integer id_product);
	
}