package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.practicas.api.entity.Tcustomer;

@Repository("CustomerRepository")
public interface CustomerRepo extends JpaRepository<Tcustomer, Serializable> {
	
	
	
	public abstract List<Tcustomer> findByNit(String nit);

}
