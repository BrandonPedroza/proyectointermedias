package com.practicas.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.Tcity;

@Repository("CityRepository")
public interface CityRepo extends JpaRepository<Tcity, Serializable> {

}
