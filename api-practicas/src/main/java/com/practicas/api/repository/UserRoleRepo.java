package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TUserRole;

@Repository("userRoleRepository")
public interface UserRoleRepo extends JpaRepository<TUserRole, Serializable> {
	
	public abstract List<TUserRole> findByIdUser(Integer idUser);

}
