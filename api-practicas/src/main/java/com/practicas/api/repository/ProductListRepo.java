package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TProductList;

@Repository("PListRepository")
public interface ProductListRepo extends JpaRepository<TProductList, Serializable> {

	public abstract List<TProductList> findByidsale(Integer idsale);
	
}