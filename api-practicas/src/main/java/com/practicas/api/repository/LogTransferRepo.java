package com.practicas.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TLogTransfer;

@Repository("logtransferRepository")
public interface LogTransferRepo extends JpaRepository<TLogTransfer, Serializable> {

}
