package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TCategory;

@Repository("CategoryRepository")
public interface CategoryRepo extends JpaRepository<TCategory, Serializable>{
	
	public abstract List<TCategory> findByIdcategory(Integer idcategory);
	
}