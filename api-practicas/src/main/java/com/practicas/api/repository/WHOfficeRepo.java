package com.practicas.api.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.TWhOffice;

@Repository("whofficeRepository")
public interface WHOfficeRepo extends JpaRepository<TWhOffice, Serializable> {

    public abstract List<TWhOffice> findByOffice(Integer office);
    
}
