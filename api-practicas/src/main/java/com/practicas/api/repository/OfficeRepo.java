package com.practicas.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practicas.api.entity.Toffice;

@Repository("OfficeRepository")
public interface OfficeRepo extends JpaRepository<Toffice, Serializable> {

}
