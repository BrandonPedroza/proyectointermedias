package com.practicas.api.component;

import java.util.Properties;

import javax.mail.Transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailsG11 {

	@Autowired
    private JavaMailSender emailSender;
	
	
	
    public void sendSimpleMessage(String to,String name, String pass) {
    	
    	Properties prop = new Properties();
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setFrom(prop.getProperty("username"));
        message.setTo(to); 
        message.setSubject("Recuperar Contraseña"); 
        message.setText(String.format("¡Estimad@ %s !\n" + 
        		"Hola , la contraseña de ingreso a tu Cuenta es:\n" + 
        		" %s \n", name,pass));
        emailSender.send(message);
      
    }
}
