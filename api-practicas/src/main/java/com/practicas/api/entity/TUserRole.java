package com.practicas.api.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "user_role", schema = "intermediasG11")
public class TUserRole  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUserRole;

	
	@Column(name = "id_user")
	private Integer idUser;

	@Column(name = "id_role")
	private Integer idRole;

}
