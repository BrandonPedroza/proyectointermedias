package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "sale",schema="intermediasG11")
public class TSale implements Serializable{


		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id_sale")
		private Integer idsale;
		
		@Column(name="id_seller")
		private Integer idSeller;
		
		@Column(name="id_customer")
		private Integer idcustomer;
		
		@Column(name="delivery_date")
		private String deliverydate;
		
		@Column(name="type")
		private Integer type;



}