package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "customer",schema="intermediasG11")
public class Tcustomer implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id_customer;
		
		@Column(name="nombre")
		private String nombre;
		
		@Column(name="nit")
		private String nit;
			
		@Column(name="dpi")
		private String dpi;
		
		@Column(name = "address")
		private String address;
		
		@Column(name = "id_office")
		private Integer id_office;


		
	

}
