package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "transfer_product",schema="intermediasG11")
public class TTransferProduct implements Serializable{

        private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        @Column(name="id_transfer_product")
        private Integer idTransferProduct;
        
        @Column(name="id_transfer")
        private Integer transfer;

        @Column(name="id_product")
        private Integer product;
        
        @Column(name="cantidad")
        private Integer cantidad;
}
