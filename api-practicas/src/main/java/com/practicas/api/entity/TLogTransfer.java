package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "log_transfer",schema="intermediasG11")
public class TLogTransfer implements Serializable{

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
                @Column(name="id_log")
        private Integer idLogTransfer;
        
		@Column(name="id_transfer")
        private Integer transfer;
        
        @Column(name="status")
		private Integer status;

        @Column(name="type")
		private Integer type;
}
