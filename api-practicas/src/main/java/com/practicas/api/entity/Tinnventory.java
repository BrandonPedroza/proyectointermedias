package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Innventory",schema="intermediasG11")
public class Tinnventory implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id_Innvetory;
		
		@Column(name="id_prod")
		private Integer id_prod;
			
		@Column(name="oldstock")
		private Integer oldstock;
		
		@Column(name = "newstock")
		private Integer newstock;
		
		@Column(name = "id_user")
		private Integer id_user;

        @Column(name="datemodify")
        private String datemodify;
        
        @Column(name = "id_warehouse")
		private Integer id_warehouse;

		
	

}
