package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "office",schema="intermediasG11")
public class Toffice implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer id_office;
		
		@Column(name="alias")
		private String alias;
			
		@Column(name="address")
		private String address;
		
		@Column(name = "id_city")
		private Integer id_city;
		
		@Column(name = "id_attendant")
		private Integer id_attendant;


		
	

}
