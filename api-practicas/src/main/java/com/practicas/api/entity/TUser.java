package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "user",schema="intermediasG11")
public class TUser implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer idUser;
		
		@Column(name="dpi")
		private Long dpi;
		
		@Column(name="name")
		private String name;
		
		@Column(name="birth_date")
		private String birth;
		
		@Column(name = "mail")
		private String mail;
		
		@Column(name = "password")
		private String password;


		
	

}
