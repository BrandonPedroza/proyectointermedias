package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "productList",schema="intermediasG11")
public class TProductList implements Serializable{


		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer idproductList;
		
		
		@Column(name="id_prod")
		private Integer idprod;
		
		@Column(name="id_sale")
		private Integer idsale;
		
		@Column(name="descripcion")
		private String descripcion;
		
		@Column(name = "valor")
		private String valor;

		@Column(name = "cantidad")
		private String cantidad;


}