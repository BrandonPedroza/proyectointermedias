package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "office_warehouse",schema="intermediasG11")
public class TWhOffice implements Serializable{

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
                @Column(name="id_office_warehouse")
                private Integer idWhOffice;
        
		@Column(name="id_office")
		private Integer office;
		
		@Column(name="id_warehouse")
		private Integer warehouse;

}
