package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "product",schema="intermediasG11")
public class TProduct implements Serializable{


		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id_product")
		private Integer idProduct;
		
		@Column(name="sku")
		private String sku;
		
		@Column(name="name")
		private String name;
		
		@Column(name="detail")
		private String detail;
		
		@Column(name = "price")
		private Float price;



}
