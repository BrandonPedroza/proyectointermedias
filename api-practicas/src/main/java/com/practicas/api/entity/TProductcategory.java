package com.practicas.api.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Product_category",schema="intermediasG11")
public class TProductcategory implements Serializable{


		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id_product_category")
		private Integer idproductcategory;
		
		
		@Column(name="id_product")
		private Integer idproduct;
		
		@Column(name="id_category")
		private Integer idcategory;



}