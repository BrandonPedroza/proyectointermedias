package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "warehouse",schema="intermediasG11")
public class TWarehouse implements Serializable{

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
        private Integer idWarehouse;
        
		@Column(name="address")
		private String address;
		
		@Column(name="status")
		private java.lang.Integer status;
		
		@Column(name = "id_attendant_wh")
		private Integer attendant;

}
