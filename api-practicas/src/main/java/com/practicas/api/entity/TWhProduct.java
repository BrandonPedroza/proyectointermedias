package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "warehouse_product",schema="intermediasG11")
public class TWhProduct implements Serializable{

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
                @Column(name="id_warehouse_product")
        private Integer idWhProduct;
        
		@Column(name="id_product")
		private Integer product;
		
		@Column(name="id_warehouse")
		private Integer warehouse;

}
