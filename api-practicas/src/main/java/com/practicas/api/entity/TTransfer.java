package com.practicas.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "transfer",schema="intermediasG11")
public class TTransfer implements Serializable{

		private static final long serialVersionUID = 1L;

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
        private Integer idTransfer;
        
		@Column(name="id_wh_in")
		private Integer in;
		
		@Column(name="id_wh_out")
        private Integer out;
        
        @Column(name="date")
		private String date;

        @Column(name="type")
		private String type;
}
