-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema intermediasG11
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `intermediasG11` ;

-- -----------------------------------------------------
-- Schema intermediasG11
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `intermediasG11` DEFAULT CHARACTER SET latin1 ;
USE `intermediasG11` ;

-- -----------------------------------------------------
-- Table `intermediasG11`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`category` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`category` (
  `id_category` INT(11) NOT NULL AUTO_INCREMENT,
  `detail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_category`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`state`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`state` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`state` (
  `idstate` INT(11) NOT NULL AUTO_INCREMENT,
  `detail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idstate`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`city`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`city` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`city` (
  `id_city` INT(11) NOT NULL AUTO_INCREMENT,
  `id_state` INT(11) NOT NULL,
  `detail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_city`),
  INDEX `id_state_idx` (`id_state` ASC),
  CONSTRAINT `id_state`
    FOREIGN KEY (`id_state`)
    REFERENCES `intermediasG11`.`state` (`idstate`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`user` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`user` (
  `id_user` INT(11) NOT NULL AUTO_INCREMENT,
  `dpi` BIGINT(13) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `birth_date` DATE NULL DEFAULT NULL,
  `mail` VARCHAR(100) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_user`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`office`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`office` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`office` (
  `id_office` INT(11) NOT NULL AUTO_INCREMENT,
  `alias` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `id_city` INT(11) NOT NULL,
  `id_attendant` INT(11) NOT NULL,
  PRIMARY KEY (`id_office`),
  INDEX `id_city_office_idx` (`id_city` ASC) ,
  INDEX `id_attendant_fk_idx` (`id_attendant` ASC) ,
  CONSTRAINT `id_attendant_fk`
    FOREIGN KEY (`id_attendant`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_city_office`
    FOREIGN KEY (`id_city`)
    REFERENCES `intermediasG11`.`city` (`id_city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`customer` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`customer` (
  `id_customer` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(250) NULL DEFAULT NULL,
  `nit` VARCHAR(45) NULL DEFAULT NULL,
  `dpi` VARCHAR(45) NULL DEFAULT NULL,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `id_office` INT(11) NOT NULL,
  PRIMARY KEY (`id_customer`),
  INDEX `id_office_cust_idx` (`id_office` ASC) ,
  CONSTRAINT `id_office_cust`
    FOREIGN KEY (`id_office`)
    REFERENCES `intermediasG11`.`office` (`id_office`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`product` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`product` (
  `id_product` INT(11) NOT NULL AUTO_INCREMENT,
  `sku` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `detail` VARCHAR(45) NULL DEFAULT NULL,
  `price` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`id_product`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`warehouse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`warehouse` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`warehouse` (
  `id_warehouse` INT(11) NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(45) NULL DEFAULT NULL,
  `status` TINYINT(1) NOT NULL,
  `id_attendant_wh` INT(11) NOT NULL,
  PRIMARY KEY (`id_warehouse`),
  INDEX `id_wh_attendant_idx` (`id_attendant_wh` ASC) ,
  CONSTRAINT `id_wh_attendant`
    FOREIGN KEY (`id_attendant_wh`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`innventory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`innventory` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`innventory` (
  `id_Innvetory` INT(11) NOT NULL AUTO_INCREMENT,
  `id_prod` INT(11) NOT NULL,
  `oldStock` INT(11) NOT NULL,
  `newStock` INT(11) NOT NULL,
  `id_user` INT(11) NOT NULL,
  `dateModify` DATE NOT NULL,
  `id_warehouse` INT(11) NOT NULL,
  PRIMARY KEY (`id_Innvetory`),
  INDEX `id_prod_inv_idx` (`id_prod` ASC) ,
  INDEX `id_user_inv_idx` (`id_user` ASC) ,
  INDEX `id_wh_stock_idx` (`id_warehouse` ASC) ,
  CONSTRAINT `id_prod_stocking`
    FOREIGN KEY (`id_prod`)
    REFERENCES `intermediasG11`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_user_invStock`
    FOREIGN KEY (`id_user`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_wh_stock`
    FOREIGN KEY (`id_warehouse`)
    REFERENCES `intermediasG11`.`warehouse` (`id_warehouse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`transfer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`transfer` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`transfer` (
  `id_transfer` INT(11) NOT NULL AUTO_INCREMENT,
  `id_wh_in` INT(11) NOT NULL,
  `id_wh_out` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_transfer`),
  INDEX `wh_in_idx` (`id_wh_in` ASC) ,
  INDEX `wh_out_idx` (`id_wh_out` ASC) ,
  CONSTRAINT `wh_in`
    FOREIGN KEY (`id_wh_in`)
    REFERENCES `intermediasG11`.`warehouse` (`id_warehouse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `wh_out`
    FOREIGN KEY (`id_wh_out`)
    REFERENCES `intermediasG11`.`warehouse` (`id_warehouse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`log_transfer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`log_transfer` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`log_transfer` (
  `id_log` INT(11) NOT NULL AUTO_INCREMENT,
  `type` INT(11) NOT NULL,
  `id_transfer` INT(11) NOT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`id_log`),
  INDEX `id_transfer_log_idx` (`id_transfer` ASC) ,
  CONSTRAINT `id_transfer_log`
    FOREIGN KEY (`id_transfer`)
    REFERENCES `intermediasG11`.`transfer` (`id_transfer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`office_warehouse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`office_warehouse` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`office_warehouse` (
  `id_office_warehouse` INT(11) NOT NULL AUTO_INCREMENT,
  `id_office` INT(11) NOT NULL,
  `id_warehouse` INT(11) NOT NULL,
  PRIMARY KEY (`id_office_warehouse`),
  INDEX `id_office_owh_idx` (`id_office` ASC) ,
  INDEX `id_wh_office_idx` (`id_warehouse` ASC) ,
  CONSTRAINT `id_office_owh`
    FOREIGN KEY (`id_office`)
    REFERENCES `intermediasG11`.`office` (`id_office`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_wh_office`
    FOREIGN KEY (`id_warehouse`)
    REFERENCES `intermediasG11`.`warehouse` (`id_warehouse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

-- -----------------------------------------------------
-- Table `intermediasG11`.`transfer_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`transfer_product` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`transfer_product` (
  `id_transfer_product` INT(11) NOT NULL AUTO_INCREMENT,
  `id_transfer` INT(11) NOT NULL,
  `id_product` INT(11) NOT NULL,
  `cantidad` INT(11) NOT NULL,
  PRIMARY KEY (`id_transfer_product`),
  INDEX `id_ptransfer_idx` (`id_transfer` ASC) ,
  INDEX `id_tproduct_idx` (`id_product` ASC) ,
  CONSTRAINT `id_ptransfer`
    FOREIGN KEY (`id_transfer`)
    REFERENCES `intermediasG11`.`transfer` (`id_transfer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_tproduct`
    FOREIGN KEY (`id_product`)
    REFERENCES `intermediasG11`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`role` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`role` (
  `id_role` INT(11) NOT NULL AUTO_INCREMENT,
  `detail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_role`))
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`privilege`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`privilege` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`privilege` (
  `id_privilege` INT(11) NOT NULL AUTO_INCREMENT,
  `id_role` INT(11) NOT NULL,
  `detail` VARCHAR(250) NULL DEFAULT NULL,
  PRIMARY KEY (`id_privilege`),
  INDEX `id_role_idx` (`id_role` ASC) ,
  CONSTRAINT `id_role`
    FOREIGN KEY (`id_role`)
    REFERENCES `intermediasG11`.`role` (`id_role`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`product_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`product_category` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`product_category` (
  `id_product_category` INT(11) NOT NULL AUTO_INCREMENT,
  `id_product` INT(11) NOT NULL,
  `id_category` INT(11) NOT NULL,
  PRIMARY KEY (`id_product_category`),
  INDEX `id_product_idx` (`id_product` ASC) ,
  INDEX `id_cat_idx` (`id_category` ASC) ,
  CONSTRAINT `id_cat`
    FOREIGN KEY (`id_category`)
    REFERENCES `intermediasG11`.`category` (`id_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_product`
    FOREIGN KEY (`id_product`)
    REFERENCES `intermediasG11`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`sale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`sale` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`sale` (
  `id_sale` INT(11) NOT NULL AUTO_INCREMENT,
  `id_customer` INT(11) NOT NULL,
  `id_seller` INT(11) NOT NULL,
  `delivery_date` DATE NOT NULL,
  `type` SMALLINT(1) NOT NULL,
  PRIMARY KEY (`id_sale`),
  INDEX `id_user_idx` (`id_seller` ASC) ,
  INDEX `id_customer_idx` (`id_customer` ASC) ,
  CONSTRAINT `id_costumer_sale`
    FOREIGN KEY (`id_customer`)
    REFERENCES `intermediasG11`.`customer` (`id_customer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_seller`
    FOREIGN KEY (`id_seller`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`product_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`product_list` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`product_list` (
  `idproduct_list` INT(11) NOT NULL AUTO_INCREMENT,
  `id_prod` INT(11) NOT NULL,
  `id_sale` INT(11) NOT NULL,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL,
  `valor` VARCHAR(45) NOT NULL,
  `cantidad` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idproduct_list`),
  INDEX `id_prod_listprod_idx` (`id_prod` ASC) ,
  INDEX `id_sale_idx` (`id_sale` ASC) ,
  CONSTRAINT `id_prod_listprod`
    FOREIGN KEY (`id_prod`)
    REFERENCES `intermediasG11`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_sale`
    FOREIGN KEY (`id_sale`)
    REFERENCES `intermediasG11`.`sale` (`id_sale`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`sale_deliverman`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`sale_deliverman` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`sale_deliverman` (
  `id_sale_deliverman` INT(11) NOT NULL AUTO_INCREMENT,
  `id_user` INT(11) NOT NULL,
  `id_sale` INT(11) NOT NULL,
  `status` SMALLINT(1) NOT NULL,
  `type` INT(11) NOT NULL,
  PRIMARY KEY (`id_sale_deliverman`),
  INDEX `id_user_delivery_idx` (`id_user` ASC) ,
  CONSTRAINT `id_user_delivery`
    FOREIGN KEY (`id_user`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`user_role` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`user_role` (
  `id_user_role` INT(11) NOT NULL AUTO_INCREMENT,
  `id_user` INT(11) NOT NULL,
  `id_role` INT(11) NOT NULL,
  PRIMARY KEY (`id_user_role`),
  INDEX `id_user_role_idx` (`id_user` ASC) ,
  INDEX `id_role_user_idx` (`id_role` ASC) ,
  CONSTRAINT `id_role_user`
    FOREIGN KEY (`id_role`)
    REFERENCES `intermediasG11`.`role` (`id_role`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_user_role`
    FOREIGN KEY (`id_user`)
    REFERENCES `intermediasG11`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `intermediasG11`.`warehouse_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `intermediasG11`.`warehouse_product` ;

CREATE TABLE IF NOT EXISTS `intermediasG11`.`warehouse_product` (
  `id_warehouse_product` INT(11) NOT NULL AUTO_INCREMENT,
  `id_product` INT(11) NOT NULL,
  `id_warehouse` INT(11) NOT NULL,
  PRIMARY KEY (`id_warehouse_product`),
  INDEX `id_product_wh_idx` (`id_product` ASC) ,
  INDEX `id_wh_prod_idx` (`id_warehouse` ASC) ,
  CONSTRAINT `id_product_wh`
    FOREIGN KEY (`id_product`)
    REFERENCES `intermediasG11`.`product` (`id_product`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `id_wh_prod`
    FOREIGN KEY (`id_warehouse`)
    REFERENCES `intermediasG11`.`warehouse` (`id_warehouse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
