const PROXY_CONFIG = [
    {
        context: [
            "/lextop/api"            
        ],
        target: "http://localhost",
        secure: false
    }
]
module.exports = PROXY_CONFIG;