import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lex-top-sidebar',
  templateUrl: './lex-top-sidebar.component.html',
  styleUrls: ['./lex-top-sidebar.component.scss']
})
export class LexTopSidebarComponent implements OnInit {
  private user:any = null;
  private rol:any = null;
  constructor() {
    if(localStorage.getItem("datosUser")){
      this.user = JSON.parse(localStorage.getItem("datosUser"))[0];
      this.rol = localStorage.getItem("roles");;
    } 
  }

  ngOnInit() {
    if(this.user){
      this.user = this.user.name;
    }
    
    

  }

}
