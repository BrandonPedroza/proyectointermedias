import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexTopSidebarComponent } from './lex-top-sidebar.component';

describe('LexTopSidebarComponent', () => {
  let component: LexTopSidebarComponent;
  let fixture: ComponentFixture<LexTopSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexTopSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexTopSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
