import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexTopNotfoundComponent } from './lex-top-notfound.component';

describe('LexTopNotfoundComponent', () => {
  let component: LexTopNotfoundComponent;
  let fixture: ComponentFixture<LexTopNotfoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexTopNotfoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexTopNotfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
