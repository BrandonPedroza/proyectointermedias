import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url:string = '/api-practicas/users/';
  constructor(private http:HttpClient, 
  private appService: UtilServiceService) 
  { }
  

  //getUsers
  getUsers(){
    const urluser=this.url+'getAllUsers';
    return this.http.get(urluser);
  }

  //insert user-api
  insertUser(dpi:string,name:string,birth:string,mail:string,password:string):Observable<any>{
    const urluser= this.url+'user';
    return this.http.post<any>(
      urluser,
      {
        "dpi":dpi,
        "name":name,
        "mail":mail,
        "birth":birth,
        "password":password
      }
    ).pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  insertRolUser(idUser:number, idRol:number){
    const urlroluser = this.url+'userRole';
    return this.http.post<any>(
      urlroluser,
      {
        "idUser":idUser,
        "idRole":idRol
      }
    ).pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  //update user->api
  updateUser(idUser:string,dpi:string,name:string,fecha_nace:string,correo:string,contrasenia:string):Observable<any>{
    const urluser = this.url+'user';
    return this.http.put<any>(
      urluser,
      {
        "idUser":idUser,
        "dpi":dpi,
        "name":name,
        "mail":correo,
        "birth":fecha_nace,
        "password":contrasenia
      }
      ).pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  //delete user - api
  deleteUser(id_user:string){
    return this.http.delete<any>(`${'/api-practicas/users/user/'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }


  //getUser login
  getLogin(username:string, password:string):Observable<any>{
    let urlLogin = this.url+'login';
    return this.http.get<any>(urlLogin,{params:{
      user:username, pass:password}})
    .pipe(
      tap(_ => console.log('fetched Usuario')),
      catchError(this.handleError<any>('getLogin'))
    );
  }

  //getrol de usuario
  getRol():Observable<any>{
    return this.http.get<any>(`${'/api-practicas/users/getAllRoles'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  //getPrivilege de usuario
  getPrivilegeByRol(idRol:number):Observable<any>{
    return this.http.get<any>(`${'/api-practicas/users/getPrivilegeByRole/'+idRol}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  //obtencion de rol del usuario
  getRolUser(idUser:number){
    return this.http.get<any>(`${'/api-practicas/users/getRolesByUser/'+idUser}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      alert(error.message);
      
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
