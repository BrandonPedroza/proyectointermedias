import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllSale(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/product/getAllSale'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createSale(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/product/sale'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateSale(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/product/sale'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteSale(idSales): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/product/sale'}`,{params:{
      idSale:idSales}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }


}

