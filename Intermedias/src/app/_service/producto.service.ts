import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from '../_service/util-service';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient,
    private appService: UtilServiceService) { }

  getAllProducts():Observable<Producto[]>{
    return this.http.get<Producto[]>(`${'/api-practicas/product/getAllProducts'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  getProductBy(idProd): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/product/getProductBy/'}`+idProd)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  // insertar 
  setProducto(producto:Producto):Observable<Producto>{
    return this.http.post<Producto>(`${'/api-practicas/product/Product'}`, producto)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  //modificar
  updateProducto(producto:Producto):Observable<any>{
    return this.http.put<any>(`${'/api-practicas/product/Product'}`, producto)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  //eliminar
  deleteProducto(producto:Producto):Observable<any>{
    return this.http.delete<any>(`${'/api-practicas/product/Product/${producto.idProduct}'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  //todas las categorias
  getCategorias():Observable<any>{
    return this.http.get<any>(`${'/api-practicas/product/getAllCategory'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  getAllPxC(producto:Producto):Observable<any>{
    return this.http.get<any>(`${'/api-practicas/product/getAllProductCategory/'}${producto.idProduct}'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  setPxC(idproducto:number, idcategoria:number):Observable<Producto>{
    let pxc = {idproduct:idproducto, idcategory:idcategoria};
    return this.http.post<Producto>(`${'/api-practicas/product/productcategory'}`, pxc)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  deleteAsignacionCat(idCXP:number):Observable<any>{
    return this.http.delete<any>(`${'/api-practicas/product/productcategory/'}${idCXP}'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }  


  /* product list */

  getAllProductList(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/product/getAllProductLists'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getAllProductListBySale(idSale): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/product/getAllProductListsBySale/'}`+idSale)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createProductList(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/product/productlist'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateProductList(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/product/productlist'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteProductList(idprodList): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/product/productlist'}`,{params:{
      idProductList:idprodList}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  
}
