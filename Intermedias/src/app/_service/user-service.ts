import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllUsers(): Observable<any> {
    return this.http.get<any>('/api-practicas/users/getAllUsers')
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getAllUserxRol(idRol:number): Observable<any> {
    return this.http.get<any>(`/api-practicas/users/getRolesUser/${idRol}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getAllRoles(): Observable<any> {
    return this.http.get<any>(`/api-practicas/users/getAllRoles/`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  
}

