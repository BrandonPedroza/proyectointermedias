import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class StateCityService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllCities(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/locaciones/getAllCities'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createCity(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/locaciones/createCity'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateCity(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/locaciones/updateCity'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteCity(idCit): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/locaciones/deleteCity'}`,{params:{
      idCity:idCit}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getAllStates(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/locaciones/getAllStates'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createState(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/locaciones/createState'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateState(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/locaciones/updateState'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteState(idSta): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/locaciones/deleteState'}`,{params:{
      idState:idSta}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }



}

