import { TestBed } from '@angular/core/testing';

import { LexTopServiceService } from './lex-top-service.service';

describe('LexTopServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LexTopServiceService = TestBed.get(LexTopServiceService);
    expect(service).toBeTruthy();
  });
});
