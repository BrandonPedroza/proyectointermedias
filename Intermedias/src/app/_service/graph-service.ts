import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllSellers(idRole): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/users/getRolesUser/'}`+idRole)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getGraphs(body): Observable<any> {

    return this.http.get<any>(`${'/api-practicas/product/getGraph'}`,
    { params: body })
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }



}

