import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllCategories(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/product/getAllCategory'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createCategory(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/product/category'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateCategory(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/product/category'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteCategory(idCat): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/product/category'}`,{params:{
      idCategory:idCat}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }



}

