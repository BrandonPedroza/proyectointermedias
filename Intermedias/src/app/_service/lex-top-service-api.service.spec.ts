import { TestBed } from '@angular/core/testing';

import { LexTopServiceApiService } from './lex-top-service-api.service';

describe('LexTopServiceApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LexTopServiceApiService = TestBed.get(LexTopServiceApiService);
    expect(service).toBeTruthy();
  });
});
