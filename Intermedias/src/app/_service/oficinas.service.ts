import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';
import { Producto } from '../models/producto';
import { City } from '../models/city';
import { Sede } from '../models/sede';
import { Bodega } from '../models/bodega';

@Injectable({
  providedIn: 'root'
})
export class OficinasService {

  constructor(private appService:UtilServiceService, private http:HttpClient) { }

  getAllOfices():Observable<any>{
    return this.http.get<any>(`${'/api-practicas/locaciones/getAllOffices'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  getAllCities():Observable<any>{
    return this.http.get<any>(`${'/api-practicas/locaciones/getAllCities'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  crearSede(sede:Sede):Observable<any>{
    return this.http.post<any>(`${'/api-practicas/locaciones/createOffice'}`, sede)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  updateSede(sede:Sede):Observable<any>{
    return this.http.put<any>(`${'/api-practicas/locaciones/updateOffice'}`, sede)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  deleteSede(sede:Sede):Observable<any>{
    return this.http.delete<any>(`${'/api-practicas/locaciones/deleteOffice/'}`+sede.id_office)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  crearBodega(bodega:Bodega):Observable<any>{
    return this.http.post<any>(`${'/api-practicas/warehouse/createWarehouse'}`, bodega)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  asignarBodega(office:number, warehouse:number):Observable<any>{
    let Obj = {office:office, warehouse:warehouse};
    return this.http.post<any>(`${'/api-practicas/warehouse/createOffice'}`, Obj)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  getBodegas(idOffice:number):Observable<any>{
    return this.http.get<any>(`${'/api-practicas/warehouse/getAllWarehouseOffice/'}${idOffice}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }
  
  getAllBodegas():Observable<any>{
    return this.http.get<any>(`${'/api-practicas/warehouse/getAllWarehouse'}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  updateWarehouse(bodega:Bodega):Observable<any>{
    return this.http.put<any>(`${'/api-practicas/warehouse/updateWarehouse'}`, bodega)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  deleteWarehouseOffice(idWarehouse:number):Observable<any>{
    return this.http.delete<any>(`${'/api-practicas/warehouse/deleteOffice/'}${idWarehouse}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }

  deleteWarehouse(idWarehouse:number):Observable<any>{
    return this.http.delete<any>(`${'/api-practicas/warehouse/deleteWarehouse/'}${idWarehouse}`)
    .pipe(
      catchError(e => this.appService.handleError(e))
    );
  }
  

}
