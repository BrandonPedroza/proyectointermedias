import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { UtilServiceService } from './util-service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(
    private http: HttpClient,
    private appService: UtilServiceService,
  ) { }

  getAllCustomers(): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/locaciones/getAllCustomers'}`)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  createCustomer(body): Observable<any> {
    return this.http.post<any>(`${'/api-practicas/locaciones/createCustomer'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  updateCustomer(body): Observable<any> {
    return this.http.put<any>(`${'/api-practicas/locaciones/updateCustomer'}`,body)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  deleteCustomer(idCust): Observable<any> {
    return this.http.delete<any>(`${'/api-practicas/locaciones/deleteCustomer'}`,{params:{
      idCustomer:idCust}})
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

  getCustomerByNit(nit): Observable<any> {
    return this.http.get<any>(`${'/api-practicas/locaciones/getCustomer/'}`+ nit)
      .pipe(
        catchError(e => this.appService.handleError(e))
      );
  }

}

