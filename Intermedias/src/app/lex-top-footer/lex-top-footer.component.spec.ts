import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexTopFooterComponent } from './lex-top-footer.component';

describe('LexTopFooterComponent', () => {
  let component: LexTopFooterComponent;
  let fixture: ComponentFixture<LexTopFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexTopFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexTopFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
