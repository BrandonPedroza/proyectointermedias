import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogProductListComponent } from './dialog-product-list.component';

describe('DialogProductListComponent', () => {
  let component: DialogProductListComponent;
  let fixture: ComponentFixture<DialogProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
