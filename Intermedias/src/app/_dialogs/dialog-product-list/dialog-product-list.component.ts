import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Producto } from 'src/app/models/producto';
import { ProductoService } from 'src/app/_service/producto.service';

declare var swal:any;

@Component({
  selector: 'app-dialog-product-list',
  templateUrl: './dialog-product-list.component.html',
  styleUrls: ['./dialog-product-list.component.scss']
})
export class DialogProductListComponent implements OnInit {
  producto:Producto;
  productList:Producto[] = [];
  formGroupInfo: FormGroup;
  data:any;
  hideButtonUpd= false;
  hideButtonAdd = false;
  hideInput=false;

  constructor(private prodSrv: ProductoService,
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogProductListComponent>,
    @Inject(MAT_DIALOG_DATA) public info: any) { }



  ngOnInit() {


    if (this.info.tipo === 1){
       console.log(this.info.prod);
        this.hideInput = true;
        this.formGroupInfo = this._formBuilder.group({
          idprod: this.info.product? this.info.prod: null,
          cantidad: this.info.product? this.info.product.cantidad: null ,
          idsale: this.info.sale.idsale
        });
      this.hideButtonUpd = true; 
      this.hideButtonAdd = false; 
    }else{
      this.hideInput = false;
      this.formGroupInfo = this._formBuilder.group({
        idprod:  null,
        cantidad: null ,
      });
      this.hideButtonUpd = false; 
      this.hideButtonAdd = true;
    }  

    this.getProducts();
  }

  getProducts(){
    this.prodSrv.getAllProducts()
    .subscribe(
      (productos:any) =>{
          this.productList = productos.recordset;
        });
      }

      add(){
        if (this.formGroupInfo.valid) {
          
         let bodyMap = {
            "idprod": this.formGroupInfo.value.idprod.idProduct,
            "idsale": this.info.prodlist.idsale,
            "descripcion": this.formGroupInfo.value.idprod.name,
            "valor": +this.formGroupInfo.value.idprod.price * this.formGroupInfo.value.cantidad,
            "cantidad": this.formGroupInfo.value.cantidad
          }
          this.prodSrv.createProductList(bodyMap)
            .subscribe(res => {
              if (res.code === 201) {
                swal.fire({
                  title: '<strong>Creación</u></strong>',
                  type: 'success',
                  text: res.msg
                });
              } else {
                swal.fire({
                  title: '<strong>Creación Fallida</u></strong>',
                  type: 'error',
                  text: res.msg
                });
              }
            })
        } 
        this.dialogRef.close();
      }
    
      update(){
        let bodyMap = {
          "idproductList": this.info.product.idproductList,
          "idprod": this.info.product.idprod,
          "idsale": this.info.product.idsale,
          "descripcion": this.formGroupInfo.value.idprod.name,
          "valor": +this.formGroupInfo.value.idprod.price * this.formGroupInfo.value.cantidad,
          "cantidad": this.formGroupInfo.value.cantidad
        }

        
      if (this.formGroupInfo.valid) {
          this.prodSrv.updateProductList(bodyMap)
            .subscribe(res => {
              if (res.code === 201) {
                swal.fire({
                  title: '<strong>Actualización</u></strong>',
                  type: 'success',
                  text: res.msg
                });
              } else {
                swal.fire({
                  title: '<strong>Actualización Fallida</u></strong>',
                  type: 'error',
                  text: res.msg
                });
              }
            })
        }
        this.dialogRef.close();
      }    
}
