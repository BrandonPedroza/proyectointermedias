import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StateCityService } from 'src/app/_service/cityState-service';
import { DialogStateComponent } from '../dialog-state/dialog-state.component';

declare var swal:any;

@Component({
  selector: 'app-dialog-city',
  templateUrl: './dialog-city.component.html',
  styleUrls: ['./dialog-city.component.scss']
})
export class DialogCityComponent implements OnInit {

  stateList:any;
  formGroupInfo: FormGroup;
  hideInput:Boolean;
  hideButtonUpd= false;
  hideButtonAdd = false;
  data:any;

  constructor(private _formBuilder: FormBuilder,
              private citySrv: StateCityService,
    public dialogRef: MatDialogRef<DialogStateComponent>,
    @Inject(MAT_DIALOG_DATA) public info: any) { }

  ngOnInit() {

    if (this.info.tipo === 1){
          this.hideInput = true;
          this.hideButtonUpd = true; 
          this.hideButtonAdd = false; 
      }else{
          this.hideInput = false;
          this.hideButtonUpd = false; 
          this.hideButtonAdd = true;
      }  

    this.data = this.info.city;
    this.formGroupInfo = this._formBuilder.group({
      idcity: this.data? this.data.idcity: null,
      idstate: this.data? this.data.idstate: null,
      detail: this.data? this.data.detail: null,
      
    });

    this.citySrv.getAllStates()
      .subscribe(res =>{
        this.stateList = res.recordset;
      })
    
  }


  add(){
    if (this.formGroupInfo.valid) {
      this.citySrv.createCity(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Creación</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Creación Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }

  update(){
    if (this.formGroupInfo.valid) {
      this.citySrv.updateCity(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Actualización</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Actualización Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }


}
