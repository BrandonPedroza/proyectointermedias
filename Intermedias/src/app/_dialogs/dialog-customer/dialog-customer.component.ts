import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from 'src/app/_service/customer-service';
import { OficinasService } from 'src/app/_service/oficinas.service';

declare var swal:any;

@Component({
  selector: 'app-dialog-customer',
  templateUrl: './dialog-customer.component.html',
  styleUrls: ['./dialog-customer.component.scss']
})
export class DialogCustomerComponent implements OnInit {

  formGroupInfo: FormGroup;
  hideInput:Boolean;
  hideButtonUpd= false;
  hideButtonAdd = false;
  officeList:any;
  data:any;

  constructor(private _formBuilder: FormBuilder,
              private officeSrv: OficinasService,
              private customerSrv: CustomerService,
    public dialogRef: MatDialogRef<DialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public info: any) { }

  ngOnInit() {

    this.officeSrv.getAllOfices()
      .subscribe(res => {
        this.officeList = res.recordset.recordset;
      });

    if (this.info.tipo === 1){
          this.hideInput = true;
          this.hideButtonUpd = true; 
          this.hideButtonAdd = false; 
      }else{
          this.hideInput = false;
          this.hideButtonUpd = false; 
          this.hideButtonAdd = true;
      }  

    this.data = this.info.customer;
    this.formGroupInfo = this._formBuilder.group({
      id_customer: this.data? this.data.id_customer: null,
      nombre: this.data? this.data.nombre: null,
      nit: this.data? this.data.nit: null ,
      dpi: this.data? this.data.dpi: null,
      address: this.data? this.data.address: null, 
      id_office: this.data? this.data.id_office:null
    });

    
  }


  add(){
    if (this.formGroupInfo.valid) {
      this.customerSrv.createCustomer(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Creación</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Creación Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }

  update(){
    if (this.formGroupInfo.valid) {
      this.customerSrv.updateCustomer(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Actualización</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Actualización Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }

}
