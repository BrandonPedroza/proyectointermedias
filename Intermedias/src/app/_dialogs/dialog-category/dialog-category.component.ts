import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CategoryService } from 'src/app/_service/category-service';
declare var swal:any;

@Component({
  selector: 'app-dialog-category',
  templateUrl: './dialog-category.component.html',
  styleUrls: ['./dialog-category.component.scss']
})
export class DialogCategoryComponent implements OnInit {

  formGroupInfo: FormGroup;
  hideInput:Boolean;
  hideButtonUpd= false;
  hideButtonAdd = false;
  data:any;

  constructor(private _formBuilder: FormBuilder,
              private categorySrv: CategoryService,
    public dialogRef: MatDialogRef<DialogCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public info: any) { }

  ngOnInit() {

    if (this.info.tipo === 1){
          this.hideInput = true;
          this.hideButtonUpd = true; 
          this.hideButtonAdd = false; 
      }else{
          this.hideInput = false;
          this.hideButtonUpd = false; 
          this.hideButtonAdd = true;
      }  

    this.data = this.info.category;
    this.formGroupInfo = this._formBuilder.group({
      idcategory: this.data? this.data.idcategory: null,
      detail: this.data? this.data.detail: null,
      
    });

    
  }


  add(){
    if (this.formGroupInfo.valid) {
      this.categorySrv.createCategory(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Creación</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Creación Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }

  update(){
    if (this.formGroupInfo.valid) {
      this.categorySrv.updateCategory(this.formGroupInfo.value)
        .subscribe(res => {
          if (res.code === 201) {
            swal.fire({
              title: '<strong>Actualización</u></strong>',
              type: 'success',
              text: res.msg
            });
          } else {
            swal.fire({
              title: '<strong>Actualización Fallida</u></strong>',
              type: 'error',
              text: res.msg
            });
          }
        })
    } 
    this.dialogRef.close();
  }


}
