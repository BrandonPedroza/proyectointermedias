import { MatPaginatorIntl } from '@angular/material/paginator'

export class MatPaginatorIntlLexTop extends MatPaginatorIntl {
    itemsPerPageLabel = 'Cantidad por página';
    nextPageLabel     = 'Siguiente';
    previousPageLabel = 'Anterior';
}
