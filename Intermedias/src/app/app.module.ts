import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppInterceptor } from './app-interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ToastrModule } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { MatInputModule } from '@angular/material/input';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginatorIntlLexTop } from './mat-paginator-intl-lex-top';

import { LexTopTemplateComponent } from './lex-top-template/lex-top-template.component';
import { LexTopFooterComponent } from './lex-top-footer/lex-top-footer.component';
import { LexTopNavbarComponent } from './lex-top-navbar/lex-top-navbar.component';
import { LexTopSidebarComponent } from './lex-top-sidebar/lex-top-sidebar.component';
import { LexTopNotfoundComponent } from './lex-top-notfound/lex-top-notfound.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatCardModule} from '@angular/material/card';
import { MatButtonModule} from '@angular/material/button';
import { MatTableModule} from '@angular/material/table';
import { MatIconModule} from '@angular/material/icon'; 
import { MatBadgeModule} from '@angular/material/badge';
import { MatDatepickerModule} from '@angular/material/datepicker'; 
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule} from '@angular/material/select'; 
import { LoginComponent } from './components/login/login.component';
import { CrudusuariosComponent } from './components/crudusuarios/crudusuarios.component';
import { RegistrouserComponent } from './components/registrouser/registrouser.component'; 
import { ProductosComponent } from './components/productos/productos.component';
import { SedeComponent } from './components/sede/sede.component';
import { BodegaComponent } from './components/bodega/bodega.component'; 
import { GraphSalesComponent } from './components/graph-sales/graph-sales.component'; 
import { CustomersComponent } from './components/customers/customers.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DialogCustomerComponent } from './_dialogs/dialog-customer/dialog-customer.component';
import { MatDialogModule} from '@angular/material/dialog';
import { MatFormFieldModule} from '@angular/material/form-field';
import { CityStateComponent } from './components/city-state/city-state.component';
import { CategoryComponent } from './components/category/category.component';
import { DialogCategoryComponent } from './_dialogs/dialog-category/dialog-category.component'; 
import { MatTabsModule} from '@angular/material/tabs';
import { DialogCityComponent } from './_dialogs/dialog-city/dialog-city.component';
import { DialogProductListComponent } from './_dialogs/dialog-product-list/dialog-product-list.component'; 
import { MatAutocompleteModule} from '@angular/material/autocomplete';
import { RegistroVentasComponent} from './components/registro-ventas/registro-ventas.component';
import { DialogStateComponent } from './_dialogs/dialog-state/dialog-state.component';
import {MatChipsModule} from '@angular/material/chips';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    LexTopTemplateComponent,
    LexTopFooterComponent,
    LexTopNavbarComponent,
    LexTopSidebarComponent,
    LexTopNotfoundComponent,
    DashboardComponent,
    CustomersComponent,
    GraphSalesComponent,
    LoginComponent,
    ProductosComponent,
    RegistrouserComponent,
    SedeComponent,
    BodegaComponent,
    CrudusuariosComponent,
    DialogCustomerComponent,
    CityStateComponent,
    CategoryComponent,
    DialogCategoryComponent,
    DialogCityComponent,
    DialogStateComponent,
    RegistroVentasComponent,
    DialogProductListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    CollapseModule.forRoot(),
    ToastrModule.forRoot(),
    PerfectScrollbarModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatBadgeModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatDialogModule,
    MatFormFieldModule,
    MatChipsModule,
    MatTabsModule,
    MatAutocompleteModule
  ],
  entryComponents: [
    DialogCustomerComponent,
    DialogCategoryComponent,
    DialogStateComponent,
    DialogCityComponent,
    DialogProductListComponent
  ],
  providers: [CookieService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlLexTop},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
