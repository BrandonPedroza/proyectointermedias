import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { UserInterface } from "../..//models/user-interface";

@Component({
  selector: 'app-registrouser',
  templateUrl: './registrouser.component.html',
  styleUrls: ['./registrouser.component.scss']
})
export class RegistrouserComponent implements OnInit {

  constructor(public userService:UsuarioService) { }
  private data:any[];
  ngOnInit() {

  }


  dpi:string="";
  name:string="";
  fecha_nace:string="";
  correo:string="";
  contrasenia:string="";
  rol:string="";
  permiso:string="";

  InsertUser(){

    console.log(this.dpi,this.name,this.fecha_nace,this.correo,this.contrasenia);

    this.userService.insertUser(this.dpi,this.name,this.fecha_nace,this.correo,this.contrasenia).subscribe((res:any) => {
      this.data = res.recordset;
      if(res.code == 201){
        alert("usuario registrado exitosamente")
        console.log("Usuario registrado exitosamente");
        console.log(res);
      }else{
        console.log("A ocurrido un error, no se realizo el registro");
      }
      
      this.dpi="";
      this.name=""
      this.correo="";
      this.contrasenia="";
      this.fecha_nace ="";
      //pendiente agregar rol y permisos
    })
  }

}
