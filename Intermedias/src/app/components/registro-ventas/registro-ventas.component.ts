import { Component, OnInit, ViewChild } from '@angular/core';
import { Venta } from '../../models/venta';
import { Detalle } from '../../models/detalle';
import { Producto } from '../../models/producto';
import { ProductoService } from '../../_service/producto.service';
import { CustomerService } from 'src/app/_service/customer-service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import * as moment from 'moment/moment';
import { SaleService } from 'src/app/_service/sale-service';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DialogProductListComponent } from 'src/app/_dialogs/dialog-product-list/dialog-product-list.component';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Router } from '@angular/router';

declare var swal: any;
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-registro-ventas',
  templateUrl: './registro-ventas.component.html',
  styleUrls: ['./registro-ventas.component.scss']
})
export class RegistroVentasComponent implements OnInit {

  customer:any;
  formGroupInfo: FormGroup;
  nameTmp:any;
  sale:any;
  prodList:any;
  showFact = false;
  usuario:any = null;

  displayedColumns: string[] = ['idproductList', 'idprod', 'idsale', 'descripcion', 'valor','cantidad','update','delete'];
  dataApp: MatTableDataSource<any>;
  dialogRef: any;
  rol:any;
  
  @ViewChild(MatSort,{ static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  constructor(private customerSrv: CustomerService,
              private saleSrv: SaleService,
              private prodSrv: ProductoService,
              private _formBuilder: FormBuilder,
              private dialog: MatDialog,
              private router:Router) {
                if(localStorage.getItem("datosUser")){
                  this.usuario = JSON.parse(localStorage.getItem("datosUser"));
                  this.rol = localStorage.getItem("roles");

                }
               }

  ngOnInit() {
    if(!this.usuario){
      this.router.navigateByUrl('login'); 
      
    }
    if(this.rol != 2){
      this.router.navigateByUrl('login');
    }
    console.log(this.usuario);
    
    
    this.formGroupInfo = this._formBuilder.group({
      nit: null,
      seller: this.usuario[0].id_user,
      sellerName: this.usuario[0].name,
      customer:null,
      date: moment(new Date()).format('YYYY-MM-DD hh:mm:ss') ,
      tipo: null
    });
    this.getAll();
  }


  findCustomer(){
    this.customerSrv.getCustomerByNit(this.formGroupInfo.value.nit)
        .subscribe(res => {
            this.customer = res.recordset[0];
            this.nameTmp = res.recordset[0].nombre;
            this.formGroupInfo.value.customer =  res.recordset[0].id_customer;
            console.log(res);
        });   
  }

  validate(){
    let bodyMap = {
      idSeller: this.formGroupInfo.value.seller,
      idcustomer:this.customer.id_customer,
      deliverydate:this.formGroupInfo.value.date,
      type: this.formGroupInfo.value.tipo 
    }
console.log(bodyMap);

 this.saleSrv.createSale(bodyMap)
        .subscribe(res => {
          this.sale = res.recordset;
        })

    this.showFact = true;    

  }


  getAll(){

    if (this.sale){
      this.prodSrv.getAllProductListBySale(this.sale.idsale)
      .subscribe(res => {
          this.prodList = res.recordset;
          this.dataApp  = new MatTableDataSource<any>(res.recordset);
          this.dataApp.paginator = this.paginator;
          console.log(this.prodList);
      })
    }

    
  }

  add(){
    
    this.dialogRef = this.dialog.open(DialogProductListComponent, {
      width: '600px',
      data: { prodlist:this.sale, tipo: 0}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
    this.getAll();
  }

  update(row){
    this.prodSrv.getProductBy(row.idprod)
          .subscribe(res => {
            this.dialogRef = this.dialog.open(DialogProductListComponent, {
              width: '600px',
              data: { product:row, prod: res.recordset.recordset, sale:this.sale, tipo: 1}  
            });
            });

    
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
    this.getAll();
  }

  delete(row){
    this.prodSrv.deleteProductList(row.idproductList)
      .subscribe(res => {
        if (res.code === 200) {
          swal.fire(
            'Eliminación',
            'Se eliminó producto correctamente',
            'success'
          );
          this.getAll();
        } else {
          swal.fire(
            'Eliminación',
            'No se eliminó producto correctamente',
            'warning'
          );
        }
      });
      this.getAll();
  }


  generateInvoice(){
    console.log("GeneratePDF");
    const documentDefinition = {
      content: [
        {
          text: 'Factura',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          alignment: 'justify',
          columns: [
            {
              text: 'Numero de factura: '+this.sale.idsale+'̣\n'
            },
            {
              text: 'Nombre de Vendedor:  Margareth Lopez ̣\n'
            }
          ]
        },
        {
          alignment: 'justify',
          columns: [
            {
              text: 'Fecha de factura: '+this.formGroupInfo.controls['date'].value+'̣\n'
            },
            {
              text: 'Nombre de Cliente: '+this.nameTmp+'̣\n'
            }
          ]
        },
        {
          alignment: 'justify',
          columns: [
            {
              text: 'Direccion de cliente: '+this.customer.address+'̣\n'
            },
            {
              text: 'Numero de nit: '+this.customer.nit+'̣\n'
            }
          ]
        },

        this.getVentasObject()
      ],
        styles: {
          header: {
            fontSize: 18,
            bold: true,
            margin: [0, 20, 0, 10],
            decoration: 'underline'
          },
          tableHeader: {
            fontSize:13,
            bold: true,
          },
          tableBody: {
            fontSize:12,
            bold: false,
            italic:true
          }
        }
    };
  
    pdfMake.createPdf(documentDefinition).open();
    
  }

  getVentasObject(){
    const ventas:any [] = [];
    this.prodList.forEach(element => {
      ventas.push(
        [
          {
            text:element.idprod,
            style:'tableBody'
  
          },
          {
            text:element.descripcion,
            style:'tableBody'
          },
          {
            text:element.cantidad,
            style:'tableBody'
          },
          {
            text:element.valor,
            style:'tableBody'
          }
        ]
      );
      
      
    });
        

      return {
        table:{
          widths:['*','*','*','*'],
          body:[
            [
              {
                text:'Codigo Producto',
                style:'tableHeader'
              },
              {
                text:'Descripcion',
                style:'tableHeader'
              },
              {
                text:'Cantidad',
                style:'tableHeader'
              },
              {
                text:'Valor',
                style:'tableHeader'
              }
            ],
            ...ventas
          ]
        }
      };

  }


}
