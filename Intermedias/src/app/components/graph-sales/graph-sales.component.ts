import { Component, OnInit } from '@angular/core';
import { GraphService } from '../../_service/graph-service';
import Chart from 'chart.js';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_service/user-service';

@Component({
  selector: 'app-graph-sales',
  templateUrl: './graph-sales.component.html',
  styleUrls: ['./graph-sales.component.scss']
})
export class GraphSalesComponent implements OnInit {

  sellersList:any;
  filters = {
    seller: '' ,
    init:'',
    end:'',
    por:'',
    tipo:''
  };

  json = {
    type: null,
    data: null,
    options: null
  }
  hex:any;

  dataStrategy = {
    datasets: [{
        label: 'Venta',
        fill: false,
        borderColor: ['#5b6582'],
        backgroundColor: ['#5b6582'],
        data: []
    }],
    labels: []
};
options = {
  scales: {
    yAxes: [{
        ticks: {
            beginAtZero:true
        }
    }]
}  
}
  private user:any = null;
  private rol:any=null;

  constructor(private graphSrv: GraphService,
              private userSrv: UserService,
              private router:Router) {
      if(localStorage.getItem("datosUser")){
        this.user = JSON.parse(localStorage.getItem("datosUser"))[0];
        this.rol = localStorage.getItem("roles");
      }
     }

  ngOnInit() {
    if(!this.user){
      this.router.navigateByUrl('login'); 

    }
    if(this.rol != 2){
      this.router.navigateByUrl('login');

    }

    this.sellers();

  }

  sellers(){

    this.userSrv.getAllRoles()
      .subscribe(res =>{
        res.recordset.forEach(element => {

          if (element.detail === 'vendedor'){
              this.graphSrv.getAllSellers(element.idRole)
              .subscribe(res => {
                this.sellersList = res.recordset;
              });
          }
          
        });
      })


    
  }

  viewGraph(){
    this.dataStrategy.labels = [];
    this.dataStrategy.datasets[0].data =[];

    let body = {
      seller: this.filters.seller ,
      init: moment(this.filters.init).format('YYYY-MM-DD'),
      end:moment(this.filters.end).format('YYYY-MM-DD'),
      por: this.filters.por
    }

  
    
    this.graphSrv.getGraphs(body)
        .subscribe(res => { 
          res.recordset.forEach(element => {
              this.dataStrategy.labels.push(element.dte);
              this.dataStrategy.datasets[0].data.push(element.total);
              let hexTmp = this.getColorHex(1).toString();
              this.dataStrategy.datasets[0].borderColor.push(hexTmp);
              this.dataStrategy.datasets[0].backgroundColor.push(hexTmp);
          });  
          this.painting();
        });
 
          
    
  }

  painting() {  
    if (this.dataStrategy.labels.length>0){
      this.json.type = this.filters.tipo;
    this.json.data = this.dataStrategy;
    this.json.options = this.options;
    new Chart('chart', this.json);
    }
  }


  getColorHex(colors: number) {
    let hex = [];
    for (let i = 0; i < colors; i++) {
    hex.push( '#' + ('000000' + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6));
    }
    return hex;
   }
}
