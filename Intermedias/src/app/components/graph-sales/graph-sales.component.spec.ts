import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphSalesComponent } from './graph-sales.component';

describe('GraphSalesComponent', () => {
  let component: GraphSalesComponent;
  let fixture: ComponentFixture<GraphSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
