import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Bodega } from '../../models/bodega';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../_service/user-service';
import { OficinasService } from 'src/app/_service/oficinas.service';

@Component({
  selector: 'app-bodega',
  templateUrl: './bodega.component.html',
  styleUrls: ['./bodega.component.scss']
})
export class BodegaComponent implements OnInit {
  bodega:Bodega;
  bodegaForm:FormGroup;
  usuarios:any [] = [];
  boegasOf:any [] = [];
  todasBodegas:Bodega[] = [];


  constructor(private route: ActivatedRoute,
    private location: Location,
    private formBuilder:FormBuilder,
    private userService:UserService,
    private oficinasService:OficinasService) {
      this.getUsuarios();
      this.bodega = new Bodega();
      this.bodega.office = +this.route.snapshot.paramMap.get('office');
      
     }

  ngOnInit() {
    this.bodegaForm = this.formBuilder.group({
      idBodega: '',
      direccionB: ['', Validators.required],
      estado: ['' , Validators.required],
      encargado:['', Validators.required]
    });
    
  }

  mostrarDatos(idbodega:number):Bodega{
    console.log(idbodega);

    var bodaux:any = this.todasBodegas.find(b => b.idWarehouse === idbodega);
    console.log(bodaux);
    return bodaux; 


  }

  mostrarUsuario(idbodega:number):string{

    var bodaux:any = this.todasBodegas.find(b => b.idWarehouse === idbodega);
    console.log(bodaux);
    var usuario = this.usuarios.find(u => u.id_user === bodaux.attendant );
    return usuario.name;

  }

  public getError(controlName:string):string{
    let error= '';
    const control = this.bodegaForm.controls[controlName];
    if(control.touched && control.errors != null){
      if(controlName == 'clave'){
        error = 'Se necesita '+controlName+' campo obligatorio, al menos 1 letra, 1 simbolo y 1 numero.';
      }else {
        error = 'Se necesita '+controlName+' campo obligatorio.';
      }

    }
    return error;

  }

  async getUsuarios(){
    const bodegueros:any =  await this.userService.getAllUserxRol(3).toPromise();
    console.log(bodegueros);
    this.usuarios = bodegueros.recordset;
    console.log(this.usuarios);

    const res0:any = await this.oficinasService.getBodegas(this.bodega.office).toPromise();
    console.log(res0);
    this.boegasOf = res0.recordset.recordset;
    console.log(this.boegasOf);
    const res:any = await this.oficinasService.getAllBodegas().toPromise();
    console.log(res);
    this.todasBodegas = res.recordset.recordset;
    console.log(this.todasBodegas);
    
  }

  dataBodega(bodega:Bodega){
    this.bodega = bodega;
    console.log(this.bodega);
    console.log("----------UPDATE-------------");

  }

  async eliminarBodega(idWarehouseOffice:number, idWarehouse:number){
    this.boegasOf = this.boegasOf.filter(b => b.idWhOffice !== idWarehouseOffice);
    this.todasBodegas = this.todasBodegas.filter(b => b.idWarehouse !== idWarehouse);
    const res:any = this.oficinasService.deleteWarehouseOffice(idWarehouseOffice).toPromise();
    console.log("------- eliminacion ------");
    console.log(res);
    
    const res2:any = this.oficinasService.deleteWarehouse(idWarehouse).toPromise();
    console.log(res2);
    console.log("------ Fin eliminacion ------");

    

  }

  async guardarB(){
    if(this.bodega.idWarehouse){
      //actualizacion de datos
      const res:any = await this.oficinasService.updateWarehouse(this.bodega).toPromise();
      console.log(res);
      alert(res.msg);

    }else {
      // ingreso
      // se crea bodega
      const respuesta:any = await this.oficinasService.crearBodega(this.bodega).toPromise();
      console.log(respuesta);
      
      console.log(this.bodega.office);
      let warehouse = respuesta.recordset.idWarehouse;

      const respuesta2:any = await this.oficinasService.asignarBodega(this.bodega.office,warehouse).toPromise();
      console.log(respuesta2);

      alert(respuesta2.msg);

      
      this.boegasOf.push(respuesta2.recordset);
      

    }

  }

  getBodegas(){

  }





}
