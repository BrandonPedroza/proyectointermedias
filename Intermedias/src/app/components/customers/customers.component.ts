import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerService } from '../../_service/customer-service';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { DialogCustomerComponent } from '../../_dialogs/dialog-customer/dialog-customer.component';
import { Router } from '@angular/router';
declare var swal:any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  
})
export class CustomersComponent implements OnInit {

  customers:any;
  displayedColumns: string[] = ['id_customer', 'nombre', 'dpi', 'nit', 'address','id_office','update','delete'];
  dataApp: MatTableDataSource<any>;
  dialogRef: any;
  private rol:any = null;
  private user:any = null;
  
  @ViewChild(MatSort,{ static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  constructor(private custSrv: CustomerService,
              private dialog: MatDialog,
              private router:Router) {
                if(localStorage.getItem("datosUser")){
                  this.user = JSON.parse(localStorage.getItem("datosUser"))[0];
                  this.rol = localStorage.getItem("roles");
                }
                
               }

  ngOnInit() {
    if(!this.user){
      this.router.navigateByUrl('login');
    }
    if(this.rol != 2){
      this.router.navigateByUrl('login');

    }
    this.getAll();
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataApp.filter = filterValue.trim().toLowerCase();

    if (this.dataApp.paginator) {
      this.dataApp.paginator.firstPage();
    }
  }


  getAll(){
    this.custSrv.getAllCustomers()
      .subscribe(res => {
          this.customers = res.recordset;
          this.dataApp  = new MatTableDataSource<any>(res.recordset);
          this.dataApp.paginator = this.paginator;
      })
  }

  add(row){
    this.dialogRef = this.dialog.open(DialogCustomerComponent, {
      width: '600px',
      data: { customer:row, tipo: 0}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
  }

  update(row){
    this.dialogRef = this.dialog.open(DialogCustomerComponent, {
      width: '600px',
      data: { customer:row, tipo: 1}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
    
  }

  delete(row){
    this.custSrv.deleteCustomer(row.id_customer)
      .subscribe(res => {
        if (res.code === 200) {
          swal.fire(
            'Eliminación',
            'Se eliminó cliente correctamente',
            'success'
          );
          this.getAll();
        } else {
          swal.fire(
            'Eliminación',
            'No se eliminó cliente correctamente',
            'warning'
          );
        }
      });
      this.getAll();
  }

}