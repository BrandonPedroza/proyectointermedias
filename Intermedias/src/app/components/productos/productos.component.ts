import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Categoria } from 'src/app/models/categoria';
import { ProductoService } from '../../_service/producto.service';
import { Producto } from '../../models/producto';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  producto:Producto;
  productoForm:FormGroup;
  productos:Producto[] = [];
  categorias:Categoria[] = [];
  ingresarCategorias:Categoria [] = [];
  cxp:any[] = [];
  ingCat:Categoria;
  
  
  constructor(private formBuild:FormBuilder,
    private productoService:ProductoService) {
    this.producto = new Producto();
    this.ingCat = new Categoria();
    
   }

  ngOnInit() {
    this.getProductos();
    this.productoForm = this.formBuild.group({
      idProducto: '',
      nombreP:['', Validators.required],
      detalleP:['', Validators.required],
      skuP:['', Validators.required],
      precioP:['',Validators.required],
      categoria:['',Validators.required]


    });
  }

  public getError(controlName:string):string{
    let error= '';
    const control = this.productoForm.controls[controlName];
    if(control.touched && control.errors != null){
      if(controlName == 'clave'){
        error = 'Se necesita '+controlName+' campo obligatorio, al menos 1 letra, 1 simbolo y 1 numero.';
      }else {
        error = 'Se necesita '+controlName+' campo obligatorio.';
      }

    }
    return error;

  }

  agregarCategoria(){
    console.log(this.ingCat);
    var tempoc2 = this.cxp.find(c => c.idcategory === this.ingCat.idcategory);
    var tempoc = this.ingresarCategorias.find(c => c === this.ingCat);
    if(tempoc || tempoc2){
      //alert('categoria ya ingresada');
      console.log('categoria ya ingresada');
    }else {
      this.ingresarCategorias.push(this.ingCat);
      console.log(this.ingresarCategorias.length);
      
    }
    this.ingCat = null;
    
  
  }

  deleteMemo(categoria:Categoria){
    this.ingresarCategorias = this.ingresarCategorias.filter(c => c !== categoria);
  }

  deleteCat(categoria:any){
    this.cxp = this.cxp.filter(c => c !== categoria);
    console.log(categoria);
    this.productoService.deleteAsignacionCat(categoria.idproductcategory)
    .subscribe(
      res=>{
        alert(res.msg);
      }
    );
    

  }

  getProductos(){
    this.productoService.getAllProducts().subscribe(
    (productos:any) =>{
        console.log(productos.recordset);
        this.productos = productos.recordset;
        console.log(this.productos);
        this.productoService.getCategorias().
         subscribe(
           (categorias:any) => {
             console.log(categorias);
             this.categorias = categorias.recordset;
           }
         );
      });

  }

  mostrarCat(idcategory:number):string{

    var categoria = this.categorias.find(c => c.idcategory === idcategory);
    return categoria.detail;

  }

  async guardarP(){
    console.log(this.producto);
    if(this.producto.idProduct){
      const res1:any = await this.productoService.updateProducto(this.producto).toPromise();
      console.log(res1);
      for(let cat of this.ingresarCategorias){
        const res2:any = await this.productoService.setPxC(this.producto.idProduct, cat.idcategory).toPromise();
        console.log(res2);

      } 
      alert(res1.msg);
      
    }else{
      const res1:any = await this.productoService.setProducto(this.producto).toPromise();
      console.log(res1);
      console.log(res1.msg);
      let idProduct = res1.recordset.idProduct;
      for(let cat of this.ingresarCategorias){
        const res2:any = await this.productoService.setPxC(idProduct, cat.idcategory).toPromise();
        console.log(res2);

      }
      
      this.productos.push(res1.recordset);

      alert(res1.msg);

    }
    this.ingresarCategorias = [];

  }

  dataProducto(prod:Producto){
    this.producto = prod;
    this.cxp = [];
    this.productoService.getAllPxC(prod).
     subscribe( catsp=>{
      console.log(catsp);
      this.cxp = catsp.recordset;
    });
  }

  async eliminarProducto(prod:Producto){
    const res0:any = await this.productoService.getAllPxC(prod).toPromise();
    console.log(res0);
    this.cxp = res0.recordset;
    for(let cpr of this.cxp){
      const res1:any = await this.productoService.deleteAsignacionCat(cpr.idproductcategory).toPromise();
      console.log(res1);
    }
    this.cxp = [];

    this.productos = this.productos.filter(producto => producto !== prod);
    const res2:any = await this.productoService.deleteProducto(prod).toPromise();
    console.log(res2);
    alert(res2.msg);


  }

}
