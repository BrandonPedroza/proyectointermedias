import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DialogCityComponent } from 'src/app/_dialogs/dialog-city/dialog-city.component';
import { DialogCustomerComponent } from 'src/app/_dialogs/dialog-customer/dialog-customer.component';
import { DialogStateComponent } from 'src/app/_dialogs/dialog-state/dialog-state.component';
import { StateCityService } from 'src/app/_service/cityState-service';

declare var swal:any;

@Component({
  selector: 'app-city-state',
  templateUrl: './city-state.component.html',
  styleUrls: ['./city-state.component.scss']
})
export class CityStateComponent implements OnInit {

  state:any;
  city:any;
  displayedColumnsState: string[] = ['idstate', 'detail', 'update','delete'];
  displayedColumnsCity: string[] = ['idcity','idstate','detail', 'update','delete'];
  dataAppState: MatTableDataSource<any>;
  dataAppCity: MatTableDataSource<any>;
  dialogRef: any;
  display:boolean;

  @ViewChild(MatSort,{ static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  constructor(private cityStaSrv: StateCityService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.getAllStates();
    this.getAllCities();
  }
  
  applyFilterState(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataAppState.filter = filterValue.trim().toLowerCase();

    if (this.dataAppState.paginator) {
      this.dataAppState.paginator.firstPage();
    }
  }

  applyFilterCity(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataAppCity.filter = filterValue.trim().toLowerCase();

    if (this.dataAppCity.paginator) {
      this.dataAppCity.paginator.firstPage();
    }
  }


  getAllStates(){
    this.cityStaSrv.getAllStates()
      .subscribe(res => {
          this.state = res.recordset;
          this.dataAppState  = new MatTableDataSource<any>(this.state);
          this.dataAppState.paginator = this.paginator;
      })
  }

  addState(row){
    this.dialogRef = this.dialog.open(DialogStateComponent, {
      width: '600px',
      data: { state:row, tipo: 0}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAllStates();
    });
  }

  updateState(row){
    console.log(row);
    
   this.dialogRef = this.dialog.open(DialogStateComponent, {
      width: '600px',
      data: { state:row, tipo: 1}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAllStates();
    });
    
  }

  deleteState(row){
    this.cityStaSrv.deleteState(row.idstate)
      .subscribe(res => {
        if (res.code === 200) {
          swal.fire(
            'Eliminación',
            'Se eliminó depto correctamente',
            'success'
          );
          this.getAllStates();
        } else {
          swal.fire(
            'Eliminación',
            'No se eliminó depto correctamente',
            'warning'
          );
        }
      });
      this.getAllStates();
  }

  getAllCities(){
    this.cityStaSrv.getAllCities()
      .subscribe(res => {
          this.dataAppCity  = new MatTableDataSource<any>(res.recordset);
          this.dataAppCity.paginator = this.paginator;
      })
  }

  addCity(row){
    this.dialogRef = this.dialog.open(DialogCityComponent, {
      width: '600px',
      data: { city:row, tipo: 0}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAllCities();
    });
  }

  updateCity(row){
    this.dialogRef = this.dialog.open(DialogCityComponent, {
      width: '600px',
      data: { city:row, tipo: 1}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAllCities();
    });
    
  }

  deleteCity(row){
    this.cityStaSrv.deleteCity(row.idcity)
      .subscribe(res => {
        if (res.code === 200) {
          swal.fire(
            'Eliminación',
            'Se eliminó cliente correctamente',
            'success'
          );
          this.getAllCities();
        } else {
          swal.fire(
            'Eliminación',
            'No se eliminó cliente correctamente',
            'warning'
          );
        }
      });
      this.getAllCities();
  }


}
