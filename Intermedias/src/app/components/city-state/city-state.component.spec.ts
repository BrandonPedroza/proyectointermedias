import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityStateComponent } from './city-state.component';

describe('CityStateComponent', () => {
  let component: CityStateComponent;
  let fixture: ComponentFixture<CityStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
