import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../../_service/usuario.service';
//import { UserInterface } from 'src/app/models/usuario';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin:FormGroup;

  constructor(private formBuilder:FormBuilder,
    private usuarioService:UsuarioService,
    private router:Router) { }

  ngOnInit() {
    
    this.formLogin = this.formBuilder.group({
      username:['', Validators.required],
      clave: ['', Validators.required],
      Rol: ['', Validators.required ]
    });
    
  }

  public getError(controlName:string):string{
    let error= '';
    const control = this.formLogin.controls[controlName];
    if(control.touched && control.errors != null){
      if(controlName == 'clave'){
        error = 'Se necesita '+controlName+' campo obligatorio, al menos 1 letra, 1 simbolo y 1 numero.';
      }else {
        error = 'Se necesita '+controlName+' campo obligatorio.';
      }

    }
    return error;

  }

  logearse(){
    let username = this.formLogin.controls['username'].value;
    let password = this.formLogin.controls['clave'].value;
    let rol = this.formLogin.controls['Rol'].value;
    console.log(username);
    console.log(password);
    console.log(rol);

    this.usuarioService.getLogin(username, password).
      subscribe(
        res =>{
          console.log(res);
          console.log(res.code);
          
          console.log(res.recordset.roles);
          for(let rol1 of res.recordset.roles){
            console.log(rol1);
            if(rol1['id_role'] == rol){
              
              if(res.code == 200){

                localStorage.setItem("datosUser", JSON.stringify(res.recordset.datos));
                localStorage.setItem("roles", rol);
                this.router.navigateByUrl('home');
                return;

              }
            } 
          }

          alert("Datos incorrectos");

        }
      );
    
  }

}
