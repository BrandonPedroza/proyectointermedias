import { Component, OnInit } from '@angular/core';
import { UsuarioService } from "../../_service/usuario.service";
import { UserInterface } from "../../models/usuario";
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';


@Component({
  selector: 'app-crudusuarios',
  templateUrl: './crudusuarios.component.html',
  styleUrls: ['./crudusuarios.component.scss']
})
export class CrudusuariosComponent implements OnInit {

  constructor(public crudUserService:UsuarioService, private fb:FormBuilder) { }
  private data:any[];
  private datarol:any[];
  private datapriv:any[];
  contactForm:FormGroup;
  selectrol:string;
  selectprivilege:string;
  temp:string[];
  temprol:string;
  viewedit:boolean = false;
  
  ngOnInit() {

    this.crudUserService.getUsers().subscribe((res:any) => {
      this.data = res.recordset.recordset;
      this.usuarios = res.recordset.recordset;
    })

    this.crudUserService.getRol().subscribe((res:any) => {
      this.datarol = res.recordset;
    })
  }

  viewroluser(){
    
  }

  printselectRol(Rol:string){
    //console.log(Rol);
    for(let r of this.datarol){
      if(r.detail == Rol){
        //console.log(parseInt(r.idRole));
        this.crudUserService.getPrivilegeByRol(parseInt(r.idRole)).subscribe((res:any) => {
          this.datapriv = res.recordset;
          //console.log(this.datapriv);
        })
      }
    }
  }


  usuarios:UserInterface[] = [];

  idUser:string="";
  dpi:string="";
  name:string="";
  fecha_nace:string="";
  correo:string="";
  contrasenia:string="";
  
  permiso:string="";

  //insert user
  InsertUser(){
    console.log(this.fecha_nace)
    this.crudUserService.insertUser(this.dpi,this.name,this.fecha_nace,this.correo,this.contrasenia).subscribe((res:any) => {
      if(res.code == 201){
        for(let r of this.datarol){
          if(r.detail == this.selectrol){
            this.insertRolUser(res.recordset.idUser,r.idRole);
            alert("Usuario creado exitosamente");
            location.reload();
          }
        }
      }
    })
  }

  insertRolUser(idUser:number,idRol:number){
    this.crudUserService.insertRolUser(idUser,idRol).subscribe((res:any) => {
      console.log("se agrego rol al usuario");
    })
  }

  UpdateUser(){
    var tempfech = moment(this.fecha_nace,'YYYY-MM-DD');
    var stringfech = tempfech.format('YYYY-MM-DD');
    //console.log(stringfech);
    this.crudUserService.updateUser(this.idUser,this.dpi,this.name,stringfech,this.correo,this.contrasenia).subscribe((res:any)=> {
      if(res.code == 201){
        for(let r of this.datarol){
          if(r.detail == this.selectrol){
            this.insertRolUser(res.recordset.idUser,r.idRole);
            alert("Usuario actualizado exitosamente");
            location.reload();
          }
        }
      }
      
    })
    
  }

  DeleteUser(idUser:string){
    this.crudUserService.deleteUser(idUser).subscribe((res:any) => {
       console.log(res);
    })
  }

  dataUser(idUser,dpi,name,fecha_nace,correo,contrasenia){
    this.viewedit = true;
    this.idUser = idUser;
    this.dpi = dpi;
    this.name = name;
    this.fecha_nace = fecha_nace;
    this.correo = correo;
    this.contrasenia = contrasenia;
    
    this.crudUserService.getRolUser(idUser).subscribe((res:any) => {
      for(let r of this.datarol){
        if(r.idRole == res.recordset[0].idRole){
          this.temprol = r.detail;
        }
      }
    })
    
  }

  //rol
  getRol(){
    this.crudUserService.getRol().subscribe((res:any) => {
      this.datarol = res.recordset.recordset;
      console.log(this.datarol)
    })
  }

  getRolUser(idUser:number){
    this.crudUserService.getRolUser(idUser).subscribe((res:any) => {
     console.log(res.recordset[0].idUser);
    })
  }

  
}
