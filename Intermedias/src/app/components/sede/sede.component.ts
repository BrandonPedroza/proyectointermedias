import { Component, OnInit } from '@angular/core';
import { Sede } from '../../models/sede';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OficinasService } from '../../_service/oficinas.service';
import { City } from '../../models/city';
import { User } from '../../models/user';
import { UserService } from '../../_service/user-service';

@Component({
  selector: 'app-sede',
  templateUrl: './sede.component.html',
  styleUrls: ['./sede.component.scss']
})
export class SedeComponent implements OnInit {
  sede:Sede;
  sedeForm:FormGroup;
  cities:City [] = [];
  usuarios:any [] = [];
  sedes:Sede[] = [];
  constructor(private formBuilder:FormBuilder,
    private oficinasService:OficinasService,
    private userService:UserService) {
    this.sede = new Sede();
   }

  ngOnInit() {
    this.getCiudades();
    this.sedeForm = this.formBuilder.group({
      idsede: '',
      aliasS: ['', Validators.required],
      direccionS: ['' , Validators.required],
      municipio: ['', Validators.required],
      encargado: ['', Validators.required]
    });
  }

  guardarS(){
    console.log(this.sede);
    if(this.sede.id_office){
      this.oficinasService.updateSede(this.sede).
       subscribe(res =>{
         alert(res.msg);
         console.log(res);

       });
    }else{
      this.oficinasService.crearSede(this.sede).
       subscribe(
        res =>{
          alert(res.msg);
          console.log(res);
          this.sedes.push(res.recordset);
        }
       );

    }

  }

  public getError(controlName:string):string{
    let error= '';
    const control = this.sedeForm.controls[controlName];
    if(control.touched && control.errors != null){
      if(controlName == 'clave'){
        error = 'Se necesita '+controlName+' campo obligatorio, al menos 1 letra, 1 simbolo y 1 numero.';
      }else {
        error = 'Se necesita '+controlName+' campo obligatorio.';
      }

    }
    return error;

  }

  dataSede(sede:Sede){
    this.sede = sede;

  }

  eliminarSede(sede:Sede){
    this.sedes = this.sedes.filter(s => s !== sede );
    this.oficinasService.deleteSede(sede).
     subscribe( se =>{
       alert(se.msg);
       console.log(se);
     });

  }

  getCiudades(){
    this.oficinasService.getAllCities().
     subscribe(
       ciudades =>{
        console.log(ciudades);
        this.cities = ciudades.recordset;
        console.log(ciudades.msg);
        this.userService.getAllUserxRol(1).
         subscribe(
          usuarios =>{
            console.log(usuarios.msg);
            console.log(usuarios);
            this.usuarios = usuarios.recordset;
            console.log("Aqui entra cuando ya este asignados");
            console.log(this.usuarios);
            this.oficinasService.getAllOfices().
             subscribe(
              sedes => {
                console.log(sedes.msg);
                console.log(sedes);
                this.sedes = sedes.recordset.recordset;
              }
             )
          }
         );
        
       }
    );
  }

  getMunicipio(idciudad:number):string{
    let ciudad:City = this.cities.find(c => c.idcity == idciudad );

    return ciudad.detail;
  }

  getEncargado(idUsuario):string{
    console.log(this.usuarios);
    console.log(idUsuario);
    let usu:any = this.usuarios.find(c => c['id_user'] == idUsuario );
    console.log(usu);

    return usu.name;
  }

}
