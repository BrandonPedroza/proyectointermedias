import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DialogCategoryComponent } from '../../_dialogs/dialog-category/dialog-category.component';
import { CategoryService } from 'src/app/_service/category-service';
declare var swal:any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  Category:any;
  displayedColumns: string[] = ['idcategory', 'detail','update','delete'];
  dataApp: MatTableDataSource<any>;
  dialogRef: any;
  display:boolean;
  
  @ViewChild(MatSort,{ static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  constructor(private catSrv: CategoryService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.getAll();
  }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataApp.filter = filterValue.trim().toLowerCase();

    if (this.dataApp.paginator) {
      this.dataApp.paginator.firstPage();
    }
  }


  getAll(){
    this.catSrv.getAllCategories()
      .subscribe(res => {
          this.Category = res.recordset;
          this.dataApp  = new MatTableDataSource<any>(res.recordset);
          this.dataApp.paginator = this.paginator;
      })
  }

  add(row){
    this.dialogRef = this.dialog.open(DialogCategoryComponent, {
      width: '600px',
      data: { category:row, tipo: 0}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
    this.getAll();
  }

  update(row){
    this.dialogRef = this.dialog.open(DialogCategoryComponent, {
      width: '600px',
      data: { category:row, tipo: 1}  
    });
    this.dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
    this.getAll();
  }

  delete(row){
    this.catSrv.deleteCategory(row.idcategory)
      .subscribe(res => {
        if (res.code === 200) {
          swal.fire(
            'Eliminación',
            'Se eliminó producto correctamente',
            'success'
          );
          this.getAll();
        } else {
          swal.fire(
            'Eliminación',
            'No se eliminó producto correctamente',
            'warning'
          );
        }
      });
      this.getAll();
  }

}
