import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class AppInterceptor implements HttpInterceptor {

    constructor(
        private cookieService: CookieService,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        const authToken = this.cookieService.get('LEXTOPSESSION');
        if(authToken){
            const authReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' +authToken)
            });
            return next.handle(authReq);
        }
        return next.handle(req);        
    }
}
