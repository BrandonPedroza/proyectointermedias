import { Component } from '@angular/core';
import { LexTopServiceService } from './_service/lex-top-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private lexTopService: LexTopServiceService
  ) { }

  ngOnInit() {

  }


}
