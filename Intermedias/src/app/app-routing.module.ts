import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { CrudusuariosComponent } from './components/crudusuarios/crudusuarios.component';
import { RegistrouserComponent } from './components/registrouser/registrouser.component';
import { ProductosComponent } from './components/productos/productos.component';
import { SedeComponent } from './components/sede/sede.component';
import { BodegaComponent } from './components/bodega/bodega.component';
import { GraphSalesComponent } from './components/graph-sales/graph-sales.component';
import { CustomersComponent } from './components/customers/customers.component';
import { CategoryComponent } from './components/category/category.component';
import { CityStateComponent } from './components/city-state/city-state.component';
import { RegistroVentasComponent } from './components/registro-ventas/registro-ventas.component';
const routes: Routes = [
  { path: '', redirectTo: '/home' , pathMatch: 'full'},
  { path: 'sale', component: RegistroVentasComponent },
  { path: 'cliente', component: CustomersComponent },
  { path: 'city', component: CityStateComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'graph', component:  GraphSalesComponent},
  { path: 'home', component:  DashboardComponent},
  { path: 'login', component: LoginComponent },
  { path: 'usuarios', component: CrudusuariosComponent},
  { path: 'producto', component: ProductosComponent},
  { path: 'sede', component:SedeComponent },
  { path: 'crudbodega/:office', component:BodegaComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
