import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexTopNavbarComponent } from './lex-top-navbar.component';

describe('LexTopNavbarComponent', () => {
  let component: LexTopNavbarComponent;
  let fixture: ComponentFixture<LexTopNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexTopNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexTopNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
