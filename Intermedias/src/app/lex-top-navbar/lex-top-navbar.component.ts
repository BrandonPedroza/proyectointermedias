import { Component, OnInit } from '@angular/core';
import { LexTopServiceService } from '../_service/lex-top-service.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

declare var Swal: any;
@Component({
  selector: 'app-lex-top-navbar',
  templateUrl: './lex-top-navbar.component.html',
  styleUrls: ['./lex-top-navbar.component.scss']
})
export class LexTopNavbarComponent implements OnInit {

  isCollapsed = true;
  private user:any = null;

  constructor(
    private cookieService: CookieService,
    private lexTopService: LexTopServiceService,
    private router:Router
    ) { 
      if(localStorage.getItem("datosUser")){
        this.user = JSON.parse(localStorage.getItem("datosUser"))[0];

      }
    }

  ngOnInit() {

  }

  toggleSidebarPin() {
    this.lexTopService.toggleSidebarPin();
  }

  toggleSidebar() {
    this.lexTopService.toggleSidebar();
  }

  salir(){
    Swal.fire({
      title: 'Cerrar sesión?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonText: 'No!',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!'
    }).then((result) => {
      if (result.value) {
        this.cookieService.deleteAll();
        localStorage.removeItem("datosUser");
        localStorage.removeItem("roles");
        this.router.navigateByUrl("login");
      }
    })
  }

}
