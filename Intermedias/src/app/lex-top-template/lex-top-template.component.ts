import { Component, OnInit } from '@angular/core';
import { LexTopServiceService } from '../_service/lex-top-service.service';

declare var jQuery: any;
@Component({
  selector: 'app-lex-top-template',
  templateUrl: './lex-top-template.component.html',
  styleUrls: ['./lex-top-template.component.scss']
})
export class LexTopTemplateComponent implements OnInit {

  constructor(private lexTopService: LexTopServiceService) { }

  ngOnInit() {
    jQuery("#main-page").css({"width":"100%", "min-height":jQuery(window).height()-250}, "max-height: auto");    
    jQuery(window).resize(function () {
      jQuery("#main-page").css({"width":"100%", "min-height":jQuery(window).height()-250}, "max-height: auto"); 
    });
  }

  getClasses() {
    const classes = {
      'pinned-sidebar': this.lexTopService.getSidebarStat().isSidebarPinned,
      'toggeled-sidebar': this.lexTopService.getSidebarStat().isSidebarToggeled
    }
    return classes;
  }
  
  toggleSidebar() {
    this.lexTopService.toggleSidebar();
  }

}
