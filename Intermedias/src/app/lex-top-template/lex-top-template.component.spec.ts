import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexTopTemplateComponent } from './lex-top-template.component';

describe('LexTopTemplateComponent', () => {
  let component: LexTopTemplateComponent;
  let fixture: ComponentFixture<LexTopTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexTopTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexTopTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
