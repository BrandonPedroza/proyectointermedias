export interface UserInterface{
    id_user:number,
    dpi:number,
    name:string,
    fecha_nace:string,
    correo:string,
    contrasenia:string,
    rol:string,
    permiso:string
}