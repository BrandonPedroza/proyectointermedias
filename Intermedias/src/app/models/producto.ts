import { DecimalPipe } from '@angular/common';

export class Producto {
    idProduct:number;
    sku:string;
    name:string;
    detail:string;
    price:number;
    idCategoria:number;
    constructor(){
        this.idProduct = 0;
        this.sku = '';
        this.name = '';
        this.detail = '';
        this.price = 0;
        this.idCategoria = 0;
    }

}
