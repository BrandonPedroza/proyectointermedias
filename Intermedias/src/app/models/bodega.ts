export class Bodega {
    idWarehouse:number;
    address:string;
    status:number;
    attendant:number;
    office:number;
    constructor(){
        this.idWarehouse = 0;
        this.address = '';
        this.status = 0;
        this.attendant = 0;
        this.office = 0;
    }
}
