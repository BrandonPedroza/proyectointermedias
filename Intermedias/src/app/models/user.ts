export class User {
    idUser:number;
    dpi:number;
    name:string;
    birth:string;
    mail:string;
    password:string;
    constructor(){
        this.idUser = 0;
        this.dpi = 0;
        this.name = '';
        this.birth = '';
        this.mail = '';
        this.password = '';
    }
}
