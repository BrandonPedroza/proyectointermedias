export class Sede {
    id_office:number;
    alias:string;
    address:string;
    id_city:number;
    id_attendant:number;

    constructor(){
        this.id_attendant = 1;
        this.alias = '';
        this.address = '';
        this.id_city = 1;
        this.id_office = 0;
    }

}
