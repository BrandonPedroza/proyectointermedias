export class Detalle {
    idproductList:number;
    idprod:number;
    idsale:number;
    descripcion:string;
    valor:string;
    cantidad:string;
    constructor(){
        this.idproductList = 0;
        this.idprod = 0;
        this.idsale = 0;
        this.descripcion = "";
        this.valor = "";
        this.cantidad = "";
    }
}
